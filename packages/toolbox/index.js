'use strict';

const { hasJsonContentType, toJson, parseBody } = require('./src/json');
const throwError = require('./src/throw-error');
const { tryCatch } = require('./src/try-catch');
const { yupValidation, yupValidationWithOptions } = require('./src/yup-validation');
const collectionsIdKeys = require('./src/collections-id-key');
const connectors = require('./src/connectors');
const createEventRequestContextInjector = require('./src/context-injector');
const createOfflineAuthoriser = require('./src/create-offline-authoriser');

module.exports = {
  ...collectionsIdKeys,
  ...connectors,
  ...throwError,
  createEventRequestContextInjector,
  createOfflineAuthoriser,
  hasJsonContentType,
  parseBody,
  toJson,
  tryCatch,
  yupValidation,
  yupValidationWithOptions
};
