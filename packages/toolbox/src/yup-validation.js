'use strict';

const { head, reduce, mergeDeepRight, propOr, unapply, converge } = require('ramda');
const { BAD_REQUEST } = require('http-status-codes');
const { curry, isNotNilOrEmpty } = require('@flybondi/ramda-land');
const { toJson } = require('./json');
const invokeOr = require('./invoke-or');

const mergeDeepAll = reduce(mergeDeepRight, {});

const propOrEmptyObject = propOr({});

/**
 * Merges together the values from `body`, `pathParameters` and `queryStringParameters`
 * from the given object. All three properties are optional. Values on `body` have prevalence
 * over `pathParameters` and `queryStringParameters`; while `pathParameters` properties
 * have priority over `queryStringParameters`.
 *
 * @function
 * @param {object} value The object containing `queryStringParameters`, `pathParameters` and/or
 *  `body` properties.
 * @returns {object} A single object with the values of the three properties merged.
 */
const extractRequestData = converge(unapply(mergeDeepAll), [
  propOrEmptyObject('queryStringParameters'),
  propOrEmptyObject('pathParameters'),
  propOrEmptyObject('body'),
  propOrEmptyObject('Records')
]);

/**
 * Curried, higher order function that wraps an `fn` functions and validates its
 * input against a provided `yup` schema. If the validations passes, it forwards
 * the call to the original handler. If it fails, it returns a `statusCode` HTTP response
 * status with a descriptive `yup` error.
 *
 * @function
 * @see https://github.com/jquense/yup
 * @param {object} yupSchema A `yup` schema descriptor.
 * @param {Function} tapOnError Callback function that will be invoked if a validation
 *  error occurred. Receives the `Error` instance, the status code
 *  and any and all original arguments to the `fn` handler function. Its return value is ignored.
 * @param {object=} options Additional configuration settings.
 * @param {Function=} option.runValidationOn A function that receives the original arguments
 *  and should return the payload to validate against the yup `schema`
 * @param {Function=} options.formatError A function that receives the error payload to be
 *  returned to the client as the body of the response and may return another object to be
 *  serialised and returned, instead.
 * @param {Function|number=} options.statusCode The HTTP status code to return on failed
 *  validation - can also be defined as a function that receives the raised error and the AWS handler
 *  arguments and returns a status code. Defaults to `400` (Bad Request).
 * @param {object=} options.yupOptions `yup` configuration options as accepted by its `yup~validate` function.
 *  (see https://github.com/jquense/yup#mixedvalidatevalue-any-options-object-promiseany-validationerror)
 * @param {Function} fn The function to wrap.
 * @returns {Function} A validation function that expects the lambda handler as
 *  its only argument.
 */
const yupValidationWithOptions = curry(function withYupValidation(
  yupSchema,
  tapOnError,
  { runValidationOn, statusCode, formatError, yupOptions = {} },
  fn
) {
  return async function validate(...args) {
    const payload = runValidationOn ? runValidationOn(...args) : extractRequestData(...args);
    try {
      // Validate input and handle possible errors
      await yupSchema.validate(payload, { abortEarly: false, ...yupOptions });
    } catch (err) {
      const status = invokeOr(statusCode, BAD_REQUEST, [err, ...args]);
      const defaultError = {
        name: err.name,
        message: `Some request parameters are missing or invalid${
          isNotNilOrEmpty(err.errors) ? `: ${head(err.errors)}` : ''
        }`,
        errors: err.errors,
        status
      };
      const errorPayload = invokeOr(formatError, defaultError, [err, status, ...args]);
      tapOnError && tapOnError(errorPayload, err, status, ...args);
      // Return an HTTP response on invalid input with appropriate error messages
      return toJson(errorPayload, { statusCode: status });
    }

    // Forward the original `event` to the wrapped handler
    return fn(...args);
  };
});

/**
 * Curried, higher order function that wraps an `fn` functions and validates its
 * input against a provided `yup` schema. If the validations passes, it forwards
 * the call to the original handler. If it fails, it returns a `400` HTTP response
 * status with a descriptive `yup` error.
 *
 * Unlike `withValidation` function, `withRequestValidation` is non configurable
 * and sets some sensible defaults: runs `yup` validation on request body payload,
 * query string and path parameters (with that order of priority for attributes with the same name);
 * uses default error format, returning an error with `name`, `message`, `errors` and `status` properties;
 * and runs `yup` validation with `abortEarly` set to `false`
 *
 * @function
 * @see https://github.com/jquense/yup
 * @param {object} schema A yup schema.
 * @param {Function} tapOnError Callback function that will be invoked if a validation
 *  error occurred. Receives the `Error` instance, the status code (always `400`)
 *  and any and all original arguments to the `fn` handler function. Its return value is ignored.
 * @param {Function} fn The function to wrap.
 * @returns {(event: object) => object} A validation function that expects the lambda handler as
 *  its only argument.
 */
const yupValidation = curry(function withYupValidation(schema, tapOnError, fn) {
  return yupValidationWithOptions(schema, tapOnError, {}, fn);
});

module.exports = {
  yupValidation
};
