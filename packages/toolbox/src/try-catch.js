'use strict';

const { curry } = require('@flybondi/ramda-land');
const { toJson } = require('./json');
const invokeOr = require('./invoke-or');

const INTERNAL_SERVER_ERROR = 500;

/**
 * Handles errors thrown by an `fn` function by returning an AWS JSON
 * API Gateway compatible response with proper information and a status code determined by
 * either the `status` property of the caught error or the result of invoking a `statusCode`]
 * function. If none of those methods provides a value, status code defaults to `500` (Internal Server Error).
 *
 * @param {(response: object, error: Error, statusCode: number, event: object?) => any} tapOnError Callback function that will be invoked on a caught error
 *  Receives  the formatted error payload (as returned by `options.formatError` or its default
 *  format if it wasn't provided), the status code, the original, unmodified thrown error instance, and
 *  any and all original arguments to the `fn` handler function. Its return value is ignored (including
 *  exceptions that might have been thrown).
 * @param {object} options Additional configuration settings.
 * @param {(number | ((error: Error) => any))=} options.statusCode Number or number returning function that
 *  receives the thrown error and must return a proper HTTP status code.
 *  Defaults to the value of `status` on the thrown error or `500` (Internal Server Error) if
 *  there was none.
 * @param {((error: Error) => any)=} options.formatError A function that receives the error payload to be
 *  returned to the client as the body of the response and may return another object to be
 *  serialised and returned, instead.
 * @param {(boolean | ((error: Error) => any))=} options.rethrow Whether to respond back with an AWS API Gateway
 *  JSON response with the formatted error payload or just re-throw the original error. As a
 *  function, `rethrow` receives the thrown error, the status code and any and all arguments to
 *  the wrapped `fn` function.
 * @param {Function} fn Errors thrown by this function will be handled. Can be async.
 * @returns {*} The result of invoking `fn` or an error payload if it raised
 *  an exception.
 */
function withTryCatchOptions(tapOnError, { statusCode, formatError, rethrow }, fn) {
  return async function tryCatch(...args) {
    try {
      return await fn(...args);
    } catch (err) {
      const status = invokeOr(statusCode, err.status || INTERNAL_SERVER_ERROR, [err, ...args]);
      const defaultError = {
        name: err.name,
        message: err.message,
        status,
        ...err
      };
      const errorPayload = invokeOr(formatError, defaultError, [err, status, ...args]);
      const shouldRethrow = invokeOr(rethrow, false, [err, status, ...args]);

      try {
        tapOnError && tapOnError(errorPayload, err, status, ...args);
      } catch (err) {
        // Ignore errors thrown by the tap function that could potentially
        // obscure the original caught error.
      }

      if (shouldRethrow) {
        // Re-raise original error if options allows for it
        // This could be useful in cases where the exception needs to
        // be caught by the underlying AWS context (for example to retry an SQS event)
        throw err;
      }
      // Return an HTTP response to the client after a raised error
      return toJson(errorPayload, { statusCode: status });
    }
  };
}

/**
 * Handles errors thrown by an `fn` function by returning an AWS JSON
 * API Gateway compatible response with proper information and a status code determined by
 * either the `status` property of the caught error or the result of invoking a `statusCode`]
 * function. If none of those methods provides a value, status code defaults to `500` (Internal Server Error).
 *
 * @example
 *
 *  tryCatchWithOptions(
 *    err => console.error('An unexpected error occurred:', err.message),
 *    { statusCode: err => err.message === 'not found' ? 400: 500 },
 *    () => {
 *      // Do stuff
 *      throw new Error('not found');
 *    }
 *  );
 *
 * @type {typeof withTryCatchOptions}
 * @function
 */
const tryCatchWithOptions = curry(withTryCatchOptions);

/**
 * Handles errors thrown by an `fn` function by returning an AWS JSON
 * API Gateway compatible response with proper information and an HTTP `500` (Internal Server Error).
 * The returned error payload contains `name`, `message` and `status` properties as well as
 * all other own properties from the original thrown error.
 *
 * @param {(response: object, error: Error, statusCode: number, event: object) => any} tapOnError tapOnError Callback function that will be invoked on a caught error
 *  Receives the `Error` instance, the status code and any and all original arguments to
 *  the `fn` handler function. Its return value is ignored.
 * @param {Function=} fn Errors thrown by this function will be handled. Can be async.
 * @returns {*} The result of invoking `fn` or an error payload if it raised
 *  an exception.
 */
function withTryCatch(tapOnError, fn) {
  return tryCatchWithOptions(tapOnError, {}, fn);
}

/**
 * * Handles errors thrown by an `fn` function by returning an AWS JSON
 * API Gateway compatible response with proper information and an HTTP `500` (Internal Server Error).
 * The returned error payload contains `name`, `message` and `status` properties as well as
 * all other own properties from the original thrown error.
 *
 * @example
 *
 *  tryCatch(
 *    err => console.error('An unexpected error occurred:', err.message),
 *    () => {
 *      // Do stuff
 *      throw new Error('not found');
 *    }
 *  );
 *
 * @type {typeof withTryCatch}
 * @function
 */
const tryCatch = curry(withTryCatch);

module.exports = {
  tryCatch
};
