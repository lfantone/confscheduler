'use strict';
const { compose, includes, propOr } = require('ramda');
const { castArray, curry, isNotNilOrEmpty } = require('@flybondi/ramda-land');
const createEventRequestContextInjector = require('./context-injector');
const { createLoggerFor, createJWTFor } = require('./connectors');
const { tryCatch } = require('./try-catch');

/**
 * @typedef {object} RequestContext
 * @property {string} accountId Request account id.
 * @property {string} apiId Request API id.
 * @property {string} httpMethod Request HTTP Method.
 * @property {string} requestId Request id.
 * @property {string} resourceId Request resource id.
 * @property {string} resourcePath Request resource path.
 * @property {string} stage Request stage.
 */

/**
 * @typedef {object} AuthoriseContext
 * @property {typeof console} console A Console proxy.
 * @property {import('./connectors/create-jwt-connector').JWTAPI} JWT The JWT API.
 * @property {string|Array<string>} resourceAudiences Requested resource allowed audience.
 */

/**
 * AWS API Gateway event.
 *
 * @typedef {object} AWSAPIGatewayEvent
 * @property {string} authorizationToken Incoming http request headers.
 * @property {AuthoriseContext} authoriserContext Auth global context.
 * @property {string} methodArn ARN of requested resource.
 * @property {RequestContext} requestContext Request global context.
 * @property {string} type Event type.
 */

/**
 * @typedef {object} Attrs Policy attributes.
 * @property {string} principalId Principal identifier.
 * @property {string=} resource The resource affected by this policy.
 * @property {string} effect Either 'Allow' or 'Deny'.
 * @property {object} context The policy context.
 */

/**
 * @typedef {object} Statement
 * @property {string} Action The policy statement action.
 * @property {string} Effect the policy statement effect.
 * @property {Array<string>} Resource The policy statement resource.
 */

/**
 * @typedef {object} Policy
 * @property {string} principalId The policy principal id.
 * @property {object} context The policy context.
 * @property {object} policyDocument The policy document.
 * @property {Array<Statement>} policyDocument.Statement The policy document statement array.
 * @property {string} policyDocument.Version The polocy document version.
 */

/**
 *Generate an allow policy document based on the given parameters.
 *
 * @param {string} resource The resource affected by this policy.
 * @param {('Allow' | 'Deny')} effect The Policy document statement effect.
 * @param {object} [context={}] The policy context.
 * @returns {Policy} An IAM policy descriptor.
 */
function generateAllowPolicy(resource, effect, context = {}) {
  return {
    principalId: 'anonymous',
    context,
    policyDocument: {
      Statement: [
        {
          Action: 'execute-api:Invoke',
          Effect: effect,
          Resource: [resource]
        }
      ],
      Version: '2012-10-17'
    }
  };
}

/**
 * Verify is incoming audience is allowed by the requested resource.
 *
 * @param {string} incomingAudience Incoming request token audience.
 * @param {string|Array<string>} resourceAudience Resource audience.
 * @returns {boolean} `true` if audience is allowed by th eresources, otherwise `false`.
 */
const isTokenAudienceValid = (incomingAudience, resourceAudience) =>
  includes(incomingAudience, castArray(resourceAudience));

/**
 *  Function that will received an incoming request and should allow or deny access to requested resource.
 *  Mock Authoriser function. Used only with serverless-offline plugin.
 *
 * @param {AWSAPIGatewayEvent} event API Gatewat event.
 * @returns {Policy} AWS Policy
 */
async function authoriseOffline(event) {
  const {
    authoriserContext: { console, JWT, resourceAudiences },
    authorizationToken,
    requestContext
  } = event;
  const { requestId, resourcePath } = requestContext;
  console.log(
    `[offline-authoriser] Received request (${requestId}) to ${resourcePath} event to authorise.`
  );
  console.log(`[offline-authoriser] Validating incoming authorisation token.`, authorizationToken);

  if (isNotNilOrEmpty(authorizationToken)) {
    const [prefix, token] = authorizationToken.split('/', 2);
    console.debug(
      `[offline-authoriser] Token prefix is ${prefix} and resource expected audiences are: ${resourceAudiences}`
    );
    if (isNotNilOrEmpty(token)) {
      try {
        const {
          payload: { email, aud, iss, admin, speaker }
        } = JWT.verify(token);

        if (isTokenAudienceValid(aud, resourceAudiences)) {
          console.log(`[offline-authoriser] Request has been authorised`);
          return generateAllowPolicy(event.methodArn, 'Allow', {
            ...requestContext,
            clientAudience: aud,
            clientSource: iss,
            user: { email, admin, speaker },
            clientToken: token
          });
        }
        console.debug(
          `[offline-authoriser] Token audience is invalid. Received ${aud} - Expect: ${resourceAudiences}`
        );
      } catch (error) {
        console.log(`[offline-authoriser] Token is invalid or malformed (${error.code})`);
        console.debug(`[offline-authoriser] ${error.message}`, error);
      }
    }

    if (isNotNilOrEmpty(prefix) && prefix === 'development') {
      // @todo: We should allow request for create user by checking the incoming request resource and with the expected by the service.
      // Allowing all with a `development/` as authToken.
      console.log(`[offline-authoriser] Request has been authorised by prefix`);
      return generateAllowPolicy(event.methodArn, 'Allow', requestContext);
    }
  }

  return generateAllowPolicy(event.methodArn, 'Deny', requestContext);
}

/**
 *
 * Returns a function that will authorise given request to access the resource.
 * This function is intended to be used as a mock authoriser function for serverless-offline plugin.
 *
 * @param {object} [config={}] The resource config.
 * @returns {(event: AWSAPIGatewayEvent) => Policy} An IAM policy desriptor.
 */
function createOfflineAuthoriser(config = {}) {
  const withLogger = createEventRequestContextInjector(() => createLoggerFor(config.logger), {
    context: 'authoriserContext'
  });
  const withJWT = createEventRequestContextInjector(() => createJWTFor(config.token), {
    context: 'authoriserContext'
  });
  const withAud = createEventRequestContextInjector(
    () => compose(castArray, propOr([], 'audience'))(config.token),
    {
      context: 'authoriserContext'
    }
  );

  return compose(
    tryCatch((res, err, statusCode, event) => {
      const {
        requestContext: { requestId }
      } = event;

      console.error(
        `[offline-authoriser][${statusCode}] An error occurred while processing the request to create a new user: ${err.message}`
      );
      console.debug(`[offline-authoriser] Request (${requestId})`, err);
    }),
    withAud(audiences => [audiences, { as: 'resourceAudiences' }]),
    withLogger(console => [console, { as: 'console' }]),
    withJWT(jwt => [jwt, { as: 'JWT' }])
  )(authoriseOffline);
}

module.exports = curry(createOfflineAuthoriser);
