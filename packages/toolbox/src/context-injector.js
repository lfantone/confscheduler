'use strict';
const { curryN, isNilOrEmpty, castArray } = require('@flybondi/ramda-land');

/**
 * AWS API Gateway event.
 *
 * @typedef {object} AWSAPIGatewayEvent
 * @property {object} headers Incoming http request headers.
 * @property {object} requestContext Request global context.
 * @property {string} path Request URL path.
 * @property {string} httpMethod HTTP Request method.
 * @property {string} queryStringParameters HTTP Request query string parameters.
 * @property {object} body HTTP Request body.
 * @property {object} context Request context.
 */

/**
 * Defines the `name` property of a function.
 *
 * @param {Function} fn Function to define a name on.
 * @param {string} name Name to set onto `fn`.
 * @returns {Function} `fn` after defining its new `name` property.
 */
function defineNameProperty(fn, name) {
  return Object.defineProperty(fn, 'name', { value: name, configurable: true });
}

/**
 * Returns a function that will extends `event.[options.context]` with a function that is the result of calling `prepareInjected`.
 *
 * @param {Function} createResource Function to create the resource injector.
 * @param {object} options Options to define the behaviour of the context enchancer.
 * @param {string} [options.context='requestContext'] Content that will be enchaced with the injected function
 * @returns {withRequestContextEnhancer} The `withRequestContextEnhancer` function.
 */
function createEventContextInjector(createResource, options = { context: 'requestContext' }) {
  return curryN(
    3,
    /**
     * Extends `event.[options.context]` with a function that is the result of calling `prepareInjected`.
     *
     * @typedef {(prepareInjected: Function, eventHandler: Function?, event: AWSAPIGatewayEvent?) => (event: AWSAPIGatewayEvent) => object} withRequestContextEnhancer
     * @param {(resource: Function, event: AWSAPIGatewayEvent, args: object?) => [Function, { as: string }]} prepareInjected A function that receives a resource and the
     *  AWS λ event (along with any additional arguments sent to the request handler) and must return
     *  a pair: an `fn` function to be injected into `event.[options.context]` and an object with an `as` property.
     *  If `as` is provided, it will be set as the `name` property of the injected function; if it's not, the
     *  function to inject must not be anonymous and have a proper `.name` value.
     * @param {(event: AWSAPIGatewayEvent, context: object?) => object} eventHandler The lambda handler function to wrap.
     * @param {AWSAPIGatewayEvent} event The AWS Lambda event descriptor.
     * @param  {*} args Additional (optional) lambda handler arguments.
     * @returns {Function} The given lambda handler with the injected context.
     */
    function withRequestContextEnhancer(prepareInjected, eventHandler, event, ...args) {
      const resource = createResource(event, ...args);

      // Create `context` if it doesn't exist already
      event[options.context] = event[options.context] || {};

      const [fn, { as } = {}] = castArray(prepareInjected(resource, event, ...args));

      // Set name of returned function to `as` if provided; if not, `fn`
      // must not be anonymous and have a valid name
      const injectedFunction = isNilOrEmpty(as) ? fn : defineNameProperty(fn, as);
      event[options.context][injectedFunction.name] = injectedFunction;
      return eventHandler(event, ...args);
    }
  );
}

module.exports = createEventContextInjector;
