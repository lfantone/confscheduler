'use strict';
const { compose, join, is, values, when, toLower } = require('ramda');

const KEY_DELIMITER_CHAR = '#';

/**
 * Creates a key value to be used for a DynamoDB record.
 * This function will join given array values or object values into a `$` separeted string.
 *
 * @example
 *   generateKeyFrom(['foo', 'bar']) // --> 'foo$bar'
 *   generateKeyFrom({ foo: 'foo', bar: 'bar' }) // --> 'foo$bar'
 *
 * @param {Array<string>|object<string, string>} value to be joined into an string.
 *
 * @returns {string} The joined string value.
 */
function generateKeyFrom(value) {
  return compose(toLower, join(KEY_DELIMITER_CHAR), when(is(Object), values))(value);
}

/**
 * Retrieve the `users` collection id to be used with DynamoDB.
 *
 * @example
 *  getUsersCollectionId() // --> 'collection#users'
 *
 * @returns {string} the generated collection id.
 */
function getUsersCollectionId() {
  return generateKeyFrom(['collection', 'users']);
}

/**
 * Creates a user sort key with the given email string value.
 *
 * @example
 *   createUsersCollectionKey('email') // -> 'user#email'
 *
 * @param {string} email user email value to create the sort key.
 * @returns {string} the generated sort key.
 */
function createUsersCollectionKey(email) {
  return generateKeyFrom(['user', email]);
}

/**
 * Retrieve the `users` collection id to be used with DynamoDB.
 *
 * @example
 *  getTalksCollectionId() // --> 'collection#talks'
 *
 * @returns {string} the generated collection id.
 */
function getTalksCollectionId() {
  return generateKeyFrom(['collection', 'talks']);
}

/**
 * Creates a user sort key with the given email string value.
 *
 * @example
 *   createTalksCollectionKey('uuid') // -> 'talks#uuid'
 *
 * @param {string} [uuid] the talk uuid value.
 * @returns {string} the generated sort key.
 */
function createTalksCollectionKey(uuid) {
  return generateKeyFrom(['talks', uuid]);
}

/**
 * Creates a user sort key with the given email string value.
 *
 * @example
 *   createStarTalksCollectionKey('uuid', 'email') // -> 'stars#uuid#user#email'
 *
 * @param {string} uuid the talk uuid value.
 * @param {string} [email] the user email value.
 * @returns {string} the generated sort key.
 */
function createStarTalksCollectionKey(uuid, email) {
  return generateKeyFrom(['stars', uuid, 'user', email]);
}

/**
 * Retrieve the `users` collection id to be used with DynamoDB.
 *
 * @example
 *  getRoomsCollectionId() // --> 'collection#rooms'
 *
 * @returns {string} the generated collection id.
 */
function getRoomsCollectionId() {
  return generateKeyFrom(['collection', 'rooms']);
}

/**
 * Creates a user sort key with the given email string value.
 *
 * @example
 *   createRoomsCollectionKey('uuid') // -> 'rooms#uuid'
 *
 * @param {string} [uuid] the talk uuid value.
 * @returns {string} the generated sort key.
 */
function createRoomsCollectionKey(uuid) {
  return generateKeyFrom(['rooms', uuid]);
}

module.exports = {
  getUsersCollectionId,
  createUsersCollectionKey,
  getTalksCollectionId,
  createTalksCollectionKey,
  createStarTalksCollectionKey,
  getRoomsCollectionId,
  createRoomsCollectionKey
};
