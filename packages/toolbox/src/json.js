'use strict';
const { rejectNilOrEmpty, isNilOrEmpty, curryN, mapKeys } = require('@flybondi/ramda-land');
const { compose, propSatisfies, contains, toLower, ifElse, F, prop } = require('ramda');

/**
 * JSON response
 *
 * @typedef {object} JSONResponse
 * @property {string} body Handler response.
 * @property {object=} headers Map of response headers.
 * @property {number=} statusCode HTTP Status code.
 */

/**
 * Checks whether the given AWS Lambda event has a `content-type` header
 * with a value containing `application/json`.
 *
 * @function
 * @param {object} event The AWS Lambda event descriptor.
 * @returns {boolean} `true` if there's a `content-type` header containing
 *  `application/json`; `false`, otherwise.
 */
const hasJsonContentType = compose(
  propSatisfies(
    ifElse(isNilOrEmpty, F, compose(contains('application/json'), toLower)),
    'content-type'
  ),
  // Normalise header names as they may be sent as either
  // `Content-Type` or `content-type`
  mapKeys(toLower),
  prop('headers')
);

/**
 * Parses `event.body` value as a JSON object if the content type for this
 * HTTP event is set to `application/json`.
 *
 * @param {(event: object) => object} eventHandler AWS Lambda event handler function.
 * @param {object} event AWS Lambda event descriptor.
 * @param {...any} args All remaining handler arguments.
 * @returns {*} The result of calling `eventHandler` with all of its arguments.
 */
function jsonBodyParser(eventHandler, event, ...args) {
  return eventHandler(
    isNilOrEmpty(event)
      ? event
      : { ...event, body: hasJsonContentType(event) ? JSON.parse(event.body) : event.body },
    ...args
  );
}

/**
 * Send a JSON response.
 *
 * @param {string|number|boolean|object} obj The response body
 * @param {object} options Additional response properties
 * @param {(number|string)=} options.statusCode The response status code
 * @param {object=} options.headers Map of response headers
 * @returns {JSONResponse} a json response object.
 */
function json(obj, { statusCode, headers } = {}) {
  const body = JSON.stringify(obj);
  return rejectNilOrEmpty({
    body,
    headers,
    statusCode
  });
}

/**
 * Send a JSON response.
 *
 * @type {typeof json}
 * @function
 */
const toJson = curryN(1, json);
/**
 * Parses `event.body` value as a JSON object if the content type for this
 * HTTP event is set to `application/json`.
 *
 * @type {typeof jsonBodyParser}
 * @function
 */
const parseBody = curryN(2, jsonBodyParser);

module.exports = {
  toJson,
  parseBody
};
