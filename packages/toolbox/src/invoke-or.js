'use strict';
const { is, isNil } = require('ramda');
const { curry } = require('@flybondi/ramda-land');

/**
 * Applies the given `args` array to the provided `fn` function and returns
 * its result. If `fn` is a literal non nil value, its value will be returned instead.
 * If `fn` is `null`, `undefined` or a function returning `null` or `undefined`,
 * `defaultValue` is returned, instead.
 *
 * @param {*} fn The function to invoke or the literal value to return.
 * @param {*} defaultValue The value to return if `fn` is `undefined`, `null` or a function
 *  returning `null` or `undefined`.
 * @param {Array<*>} args A set of arguments to apply to `fn` (if it's a function).
 * @returns {*} If `fn` is a function, returns the result of applying `args` to `fn`; if `fn`
 *  if `null` or `undefined` or returns `null` or `undefined`, `invokeOr` returns `defaultValue`,
 */
function invokeWith(fn, defaultValue, args) {
  const status = is(Function, fn) || typeof fn === 'function' ? fn(...args) : fn;
  return isNil(status) ? defaultValue : status;
}

/**
 * Applies the given `args` array to the provided `fn` function and returns
 * its result. If `fn` is a literal non nil value, its value will be returned instead.
 * If `fn` is `null`, `undefined` or a function returning `null` or `undefined`,
 * `defaultValue` is returned, instead.
 *
 * @type {typeof invokeWith}
 * @function
 */
const invokeOr = curry(invokeWith);

module.exports = invokeOr;
