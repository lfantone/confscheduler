'use strict';
const { prop, unless, curryN } = require('ramda');
const { BAD_REQUEST, NOT_FOUND } = require('http-status-codes');

class HTTPError extends Error {
  constructor(message, code = BAD_REQUEST) {
    super(`${[code]} ${message}`);
    this.status = code;
    this.name = this.constructor.name;
    if (typeof Error.captureStackTrace === 'function') {
      Error.captureStackTrace(this, this.constructor);
    } else {
      this.stack = new Error(this.message).stack;
    }
  }
}

/**
 * Inmediatyle throws an `HTTPError` with the given `message` and `code`.
 *
 * @example
 *  throwError('boom', 500); // --> throw `new HTTPError('boom')`
 *
 * @param {string} message The error message
 * @param {number} code The HTTP status code.
 * @throws {HTTPError}
 */
function throwError(message, code) {
  throw new HTTPError(message, code);
}

/**
 * Throws an error unless the `propName` property is not nil or empty in the given obj.
 *
 * @example
 *  throwErrorIfPropIsNil('FORBIDDEN', 'id', { id: null })  // --> throws FORBIDDEN error
 *  throwErrorIfPropIsNil({ id: 'foo' })  // --> nothing!
 *
 * @function
 * @param {string} message The error message to use in the error.
 * @param {string=} propName The object property name to check if is nil.
 * @param {number=} code The HTTP status code to use in the error.
 * @param {object=} obj object to get the `id` property.
 * @throws {HTTPError}
 * @returns {object} The given obj
 */
function throwErrorIfPropIsNil(message, propName, code, obj) {
  return unless(prop(propName), () => throwError(message, code), obj);
}

/**
 * Throws an error unless the `propName` property is not nil or empty in the given obj.
 * Curried version of `throwErrorIfPropIsNil`.
 *
 * @example
 *
 *  throwErrorIf('foo', 'bar', 500, { bar: undefined }) // --> Throws INTERNAL SERVER ERROR error
 *  throwErrorIf('foo', 'nothing', 500, { nothing: 'foo' }) // --> { nothing: 'foo' }
 *
 * @type {typeof throwErrorIfPropIsNil}
 * @function
 */
const throwErrorIf = curryN(4, throwErrorIfPropIsNil);

/**
 * Throws an error unless the `resource.id` property is not nil or empty in the context.
 *
 * @example
 *  throwErrorIfResourceIsNilOrEmpty({ id: null })  // --> throws NOT_FOUND error
 *  throwErrorIfResourceIsNilOrEmpty({ id: 'foo' })  // --> nothing!
 *
 * @function
 * @param {object} resource object to get the `id` property
 * @type {typeof throwErrorIf}
 * @throws {Error}
 */
const throwErrorIfResourceIsNilOrEmpty = throwErrorIf("Resource doesn't exist", 'id', NOT_FOUND);

module.exports = {
  throwError,
  throwErrorIf,
  throwErrorIfResourceIsNilOrEmpty
};
