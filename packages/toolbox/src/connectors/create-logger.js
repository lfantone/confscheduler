'use strict';
const { always } = require('ramda');

const emptyString = always('');

/**
 * Enable/Disable `console.debug` function based on configured loggin level.
 *
 * @param {string} level logging level value
 * @returns {typeof console} A Console proxy.
 */
function createLoggerFor(level) {
  return new Proxy(
    {},
    {
      get: (obj, service) => {
        if (!obj[service]) {
          switch (level) {
            case 'debug':
              obj[service] = service === 'debug' ? console.log : console[service];
              break;

            default:
              obj[service] = service === 'debug' ? emptyString : console[service];
              break;
          }
        }
        return obj[service];
      }
    }
  );
}

module.exports = createLoggerFor;
