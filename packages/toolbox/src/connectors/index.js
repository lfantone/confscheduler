'use strict';

const createFlynamoConnectorFor = require('./create-flynamo-connector');
const createJWTFor = require('./create-jwt-connector');
const createLoggerFor = require('./create-logger');

module.exports = {
  createFlynamoConnectorFor,
  createJWTFor,
  createLoggerFor
};
