'use strict';

const AWS = require('@confscheduler/resources/src/aws-sdk');
const createFlynamo = require('@flybondi/flynamo');

/**
 * Represents the amount of provisioned throughput capacity consumed on a table or an index.
 *
 * @typedef {object} Capacity
 * @property {number} CapacityUnits The total number of capacity units consumed on a table or an index.
 * @property {number} ReadCapacityUnits The total number of read capacity units consumed on a table or an index.
 * @property {number} WriteCapacityUnits The total number of write capacity units consumed on a table or an index.
 */

/**
 *
 * @typedef {object} ConsumedCapacity
 * @property {number} CapacityUnits The total number of capacity units consumed by the operation.
 * @property {string} GlobalSecondaryIndexes The amount of throughput consumed on each global index affected by the operation.
 * @property {string} LocalSecondaryIndexes The amount of throughput consumed on each local index affected by the operation.
 * @property {number} ReadCapacityUnits The total number of read capacity units consumed by the operation.
 * @property {Capacity} Table The amount of throughput consumed on the table affected by the operation.
 * @property {string} TableName The name of the table that was affected by the operation.
 * @property {number} WriteCapacityUnits The total number of write capacity units consumed by the operation.
 */

/**
 * @typedef {object} DynamoDBResponse
 * @property {Array<ConsumedCapacity>} ConsumedCapacity The capacity units consumed by the operation
 * @property {object} Item A map of attribute names to AttributeValue objects, as specified by ProjectionExpression.
 * @property {Array<object>} Items An array of item attributes that match the scan criteria. Each element in this array consists of an attribute name and the value for that attribute.
 * @property {number} Count The number of items in the response.
 * @property {object} LastEvaluatedKey The primary key of the item where the operation stopped, inclusive of the previous result set. Use this value to start a new operation, excluding this value in the new request.
 * @property {number} ScannedCount The number of items evaluated, before any ScanFilter is applied.
 */

/**
 * @typedef {object} FlynamoAPI
 * @property {(payload: object) => Promise<object>} batchGet The `BatchGetItem` operation retrieves multiple items from a table in a single call.
 * @property {(payload: object) => Promise<object>} batchInsert The `BatchWriteItem` operation puts or deletes multiple items in one table.
 * @property {(payload: object) => Promise<object>} batchRemove The `BatchWriteItem` operation puts or deletes multiple items in one table.
 * @property {(payload: object) => Promise<object>} batchWrite The `BatchWriteItem` operation puts or deletes multiple items in one table.
 * @property {(payload: object) => Promise<number>} count Return the number of elements in a table or a secondary index.
 * @property {(payload: object) => Promise<object>} get The `GetItem` operation returns a set of attributes for the item with the given primary key.
 * @property {(payload: object) => Promise<Array<object>>} getAll The `Scan` operation returns one or more items and item attributes by accessing every item in a table or a secondary index. To have DynamoDB return fewer items, you can provide a FilterExpression operation.
 * @property {(payload: object) => Promise<object>} insert Create new items, or replace old items with new ones.
 * @property {(payload: object) => Promise<Array<object>>} query The `Query` operation finds items based on primary key values.
 * @property {(payload: object) => Promise<object>} remove Deletes a single item in a table by primary key.
 * @property {(payload: object, request: object) => Promise<object>} update Edit an existing item's attributes, or add a new item to a table if it does not already exist.
 */

/**
 * Returns a Flynamo API to connect to the DB and interact with the given table name
 *
 * @param {string} table the table name to use with Flynamo.
 * @returns {FlynamoAPI} an object with the Flynamo API.
 */
function createFlynamoConnectorFor(table) {
  const { forTable } = createFlynamo(new AWS.DynamoDB());

  return forTable(table);
}

module.exports = createFlynamoConnectorFor;
