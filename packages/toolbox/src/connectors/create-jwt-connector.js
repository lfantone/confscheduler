'use strict';

const { curry } = require('@flybondi/ramda-land');
const { JWK, JWT } = require('jose');

/**
 * @typedef {object} JWTAPI
 * @property {function(object): string} sign Serialises and signs the payload as JWT.
 * @property {function(string): object} verify Verifies the claims and signature of a JSON Web Token.
 * @property {function(string): object} decode Decodes the JWT payload and optionally the header. Does not perform any claim validations what so ever.
 */

/**
 * Generates a new JWT token from the given payload with given key.
 * This method is a wrapper of jose.JWT implementation.
 * For more information check https://github.com/panva/jose/blob/HEAD/docs/README.md#jwtsignpayload-key-options
 *
 * @param {JWK.Key} key The key to sign with.
 * @param {object} options the optional parameter config.
 * @param {string} options.expiresIn JWT Expiration Time.
 * @param {string} options.issuer JWT Issuer.
 * @param {string|Array<string>} options.audience JWT Audience.
 * @param {object} payload JWT Claims Set.
 * @returns {string} the generated token
 */
function createWebToken(key, options, payload) {
  return JWT.sign(payload, key, options);
}

/**
 * Curried function of `createWebToken`.
 *
 * @type {typeof createWebToken}
 */
const createWebTokenWith = curry(createWebToken);

/**
 * Verifies the claims and signature of a JSON Web Token.
 * This method is a wrapper of jose.JWT implementation.
 * For more information check https://github.com/panva/jose/blob/HEAD/docs/README.md#jwtverifytoken-keyorstore-options
 *
 * @param {JWK.Key} key The key to sign with.
 * @param {object} options the optional parameter config.
 * @param {string} options.issuer JWT Issuer.
 * @param {string|Array<string>} options.audience JWT Audience.
 * @param {string} token JSON Web Token to verify.
 * @returns {object} the token payload.
 */
function verifyWebToken(key, options, token) {
  return JWT.verify(token, key, options);
}

/**
 * Curried function of `verifyWebToken`
 *
 * @type {typeof verifyWebToken}
 */
const verifyWebTokenWith = curry(verifyWebToken);

/**
 * Imports a symmetric key.
 * This method is a wrapper of jose.JWT implementation.
 * For more information check https://github.com/panva/jose/blob/HEAD/docs/README.md#jwkaskeysecret-options-secret-key-import
 *
 * @param {string} secret the value used to generate the key.
 * @returns {JWK.OctKey} the generated symmetric key.
 */
function generateOctKeyFrom(secret) {
  return JWK.asKey(Buffer.from(secret), { kty: 'oct', use: 'sig' });
}

/**
 * Creates the JWT API.
 *
 * @param {object} payload with the config to create the API.
 * @param {string} payload.secret the value used to generate the key.
 * @param {string} payload.expiresIn JWT Expiration Time.
 * @param {string} payload.issuer JWT Issuer.
 * @param {string|Array<string>} payload.audience JWT Audience.
 * @returns {JWTAPI} an object with the JWT API.
 */
function createJWTFor({ secret, expiresIn, issuer, audience }) {
  const key = generateOctKeyFrom(secret);

  return {
    sign: createWebTokenWith(key, { expiresIn, issuer, audience }),
    verify: verifyWebTokenWith(key, { issuer, audience, complete: true }),
    decode: JWT.decode
  };
}

module.exports = createJWTFor;
