module.exports = {
  extends: ['../../eslint.config.js', 'plugin:vue/essential', '@vue/prettier'],
  parserOptions: {
    parser: 'babel-eslint'
  }
};
