import Vue from 'vue';
import VueRouter from 'vue-router';
import { pathOr } from 'ramda';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Home',
    component: () => import(/* webpackChunkName: "homr" */ '../views/Home.vue'),
    meta: { authenticated: true }
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import(/* webpackChunkName: "login" */ '../views/Login.vue'),
    meta: { authenticated: false }
  },
  {
    path: '/talks/:talkId',
    name: 'Talk',
    component: () => import(/* webpackChunkName: "talk" */ '../views/Talk.vue'),
    meta: { authenticated: true }
  },
  {
    path: '/profile',
    name: 'Profile',
    component: () => import(/* webpackChunkName: "talk" */ '../views/Profile.vue'),
    meta: { authenticated: true }
  }
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});

const getAuthenticated = pathOr(true, ['meta', 'authenticated']);

function authMiddleware(to, from, next) {
  const { matched } = to;
  if (matched.some(getAuthenticated)) {
    if (!Vue.$cookies.isKey('token')) {
      return next({
        path: '/login',
        query: { redirect: to.fullPath }
      });
    }
  }
  return next();
}

router.beforeEach(authMiddleware);

export default router;
