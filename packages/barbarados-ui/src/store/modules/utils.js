import {
  compose,
  filter,
  includes,
  map,
  pluck,
  prop,
  propEq,
  startsWith,
  toLower,
  uniq
} from 'ramda';
import { mergeSpec } from '@flybondi/ramda-land';
import { format, parseISO } from 'date-fns/fp';

const TIME_FORMAT = 'HH:mm';
const DATE_FORMAT = 'eeee, LLLL dd';

export const startWithNotAuthorized = compose(includes('not authorized'), toLower, prop('message'));
export const startWithForbidden = compose(startsWith('forbidden'), toLower, prop('message'));
export const startWith404 = compose(startsWith('404 resource'), toLower, prop('message'));
export const formatDateAndTime = mergeSpec({
  date: compose(format(DATE_FORMAT), parseISO, prop('time')),
  time: compose(format(TIME_FORMAT), parseISO, prop('time'))
});
export const formatTimePropFromAll = map(formatDateAndTime);
export const generateSchedulesFrom = compose(
  uniq,
  map(compose(format(DATE_FORMAT), parseISO)),
  pluck('time')
);
export const filterTalksById = (talks, talkId) => filter(propEq('talkId', talkId), talks);
