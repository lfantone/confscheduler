import { propOr, pathOr } from 'ramda';
import { LOGIN, SET_ERROR, LOGOUT, SET_USER, SET_USER_STARRED_TALKS } from '../types';
import { authenticate, getUserBy } from '@/api/users';
import { getTalk } from '@/api/talks';
import { startWithForbidden, startWith404, formatTimePropFromAll } from './utils';

const state = {
  authenticated: null,
  user: null,
  token: null,
  error: null,
  starred: null
};

const getters = {
  getUserEmail: state => pathOr(window.$cookies.get('user'), ['user', 'email'], state),
  getUserToken: state => propOr(window.$cookies.get('token'), 'token', state),
  getUserImage: pathOr(null, ['user', 'image']),
  getUserStars: pathOr([], ['user', 'stars']),
  getUserName: pathOr(null, ['user', 'name']),
  getUserStarredTalks: propOr([], 'starred'),
  isAdmin: pathOr(false, ['user', 'admin']),
  isAuthenticated: state => propOr(window.$cookies.isKey('token'), 'authenticated', state),
  isSpeaker: pathOr(false, ['user', 'speaker'])
};

const mutations = {
  [LOGIN]: (state, { token, email }) => {
    state.authenticated = true;
    state.token = token;
    state.user = { email };
    window.$cookies.set('token', token);
    window.$cookies.set('user', email);
  },
  [SET_ERROR]: (state, error) => {
    state.error = error;
  },
  [LOGOUT]: state => {
    state.authenticated = false;
    state.user = null;
    state.token = null;
    window.$cookies.remove('token');
    window.$cookies.remove('user');
  },
  [SET_USER]: (state, user) => {
    state.user = user;
  },
  [SET_USER_STARRED_TALKS]: (state, talks) => {
    state.starred = formatTimePropFromAll(talks);
  }
};

const actions = {
  setError: ({ commit }, error) => {
    commit(SET_ERROR, error);
  },
  authenticate: async ({ commit }, email) => {
    try {
      const { token, errors } = await authenticate(email);

      errors && errors.length > 0 ? commit(SET_ERROR, errors[0]) : commit(LOGIN, { token, email });
    } catch (error) {
      commit(SET_ERROR, error.message);
    }
  },
  getCurrentUser: async ({ state, commit, getters }) => {
    try {
      const { getUserEmail: email, getUserToken: token } = getters;
      const user = await getUserBy({ email, token });
      commit(SET_USER, user);
    } catch (error) {
      if (startWithForbidden(error) || startWith404(error)) {
        commit(LOGOUT);
      } else {
        commit(SET_ERROR, error.message);
      }
    }
  },
  getUserStarredTalks: async ({ state, commit, dispatch, getters }) => {
    try {
      const { getUserStars: starredTalkIds, getUserToken: token } = getters;
      const talks = await Promise.all(starredTalkIds.map(id => getTalk(token, id)));
      commit(SET_USER_STARRED_TALKS, talks);
    } catch (error) {
      if (startWithForbidden(error) || startWith404(error)) {
        commit(LOGOUT);
      } else {
        commit(SET_ERROR, error.message);
      }
    }
  },
  logout: ({ commit }) => {
    commit(LOGOUT);
  }
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
};
