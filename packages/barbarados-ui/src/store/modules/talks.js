import { compose, groupBy, prop, propOr } from 'ramda';
import { isNilOrEmpty, rejectNilOrEmpty } from '@flybondi/ramda-land';
import {
  SET_TALK,
  SET_TALKS,
  SET_TALKS_ERROR,
  SET_TALKS_SCHEDULE_DATE,
  LOGOUT,
  GENERATE_SCHEDULE
} from '../types';
import { getTalk, getAllTalks, addStarToTalk, deleteStarFromTalk, updateTalk } from '@/api/talks';
import {
  startWithNotAuthorized,
  formatDateAndTime,
  formatTimePropFromAll,
  generateSchedulesFrom,
  filterTalksById
} from './utils';

const state = {
  error: null,
  talk: null,
  talks: null,
  scheduleDate: null,
  schedule: []
};

const getters = {
  getTalk: propOr(null, 'talk'),
  getTalks: propOr([], 'talks'),
  getTalksGroupByDate: compose(groupBy(prop('date')), propOr([], 'talks')),
  getSchedule: propOr([], 'schedule'),
  getSelectedDate: prop('scheduleDate')
};

const mutations = {
  [SET_TALK]: (state, { talk, format }) => {
    state.talk = format ? formatDateAndTime(talk) : talk;
  },
  [SET_TALKS]: (state, talks) => {
    state.talks = formatTimePropFromAll(talks);
  },
  [SET_TALKS_SCHEDULE_DATE]: (state, date) => {
    state.scheduleDate = date;
  },
  [SET_TALKS_ERROR]: (state, message) => {
    state.error = message;
  },
  [GENERATE_SCHEDULE]: (state, talks) => {
    const schedule = generateSchedulesFrom(talks);
    state.schedule = schedule;
    state.scheduleDate = schedule[0] ?? null;
  }
};

const actions = {
  getTalk: async ({ state, commit, dispatch, rootGetters }, talkId) => {
    const { talks } = state;
    if (isNilOrEmpty(talks)) {
      try {
        const token = rootGetters['user/getUserToken'];
        const talk = await getTalk(token, talkId);

        commit(SET_TALK, { talk, format: true });
      } catch (error) {
        if (startWithNotAuthorized(error)) {
          dispatch(`user/${LOGOUT}`, {}, { root: true });
        } else {
          commit(SET_TALKS_ERROR, error.message);
        }
      }
    } else {
      const [talk] = filterTalksById(talks, talkId);
      commit(SET_TALK, { talk, format: false });
    }
  },
  getTalks: async ({ commit, dispatch, rootGetters }) => {
    try {
      const token = rootGetters['user/getUserToken'];
      const talks = await getAllTalks(token);

      commit(SET_TALKS, talks);
      commit(GENERATE_SCHEDULE, talks);
    } catch (error) {
      if (startWithNotAuthorized(error)) {
        dispatch(`user/${LOGOUT}`, {}, { root: true });
      } else {
        commit(SET_TALKS_ERROR, error.message);
      }
    }
  },
  addStar: async ({ commit, rootGetters, dispatch }, talkId) => {
    try {
      const token = rootGetters['user/getUserToken'];
      const isAdmin = rootGetters['user/isAdmin'];

      await addStarToTalk(token, talkId);

      dispatch('user/getCurrentUser', {}, { root: true });
      isAdmin && dispatch('getTalks');
    } catch (error) {
      if (startWithNotAuthorized(error)) {
        dispatch(`user/${LOGOUT}`, {}, { root: true });
      } else {
        commit(SET_TALKS_ERROR, error.message);
      }
    }
  },
  removeStar: async ({ commit, rootGetters, dispatch }, talkId) => {
    try {
      const token = rootGetters['user/getUserToken'];
      await deleteStarFromTalk(token, talkId);

      dispatch('user/getCurrentUser', {}, { root: true });
    } catch (error) {
      if (startWithNotAuthorized(error)) {
        dispatch(`user/${LOGOUT}`, {}, { root: true });
      } else {
        commit(SET_TALKS_ERROR, error.message);
      }
    }
  },
  setScheduleDate: ({ commit }, date) => {
    commit(SET_TALKS_SCHEDULE_DATE, date);
  },
  updateTalk: async ({ commit, rootGetters, dispatch }, talk) => {
    try {
      const token = rootGetters['user/getUserToken'];
      const updatedTalk = await updateTalk(token, rejectNilOrEmpty(talk));

      commit(SET_TALK, { talk: updatedTalk, format: true });
    } catch (error) {
      if (startWithNotAuthorized(error)) {
        dispatch(`user/${LOGOUT}`, {}, { root: true });
      } else {
        commit(SET_TALKS_ERROR, error.message);
      }
    }
  }
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
};
