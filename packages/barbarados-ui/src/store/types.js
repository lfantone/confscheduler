export const LOGIN = 'login';
export const LOGOUT = 'logout';
export const SET_ERROR = 'set-error';
export const SET_USER = 'set-user';
export const SET_USER_STARRED_TALKS = 'set-starred-talks';

export const SET_TALK = 'set-talk';
export const SET_TALKS = 'set-talks';
export const SET_TALKS_ERROR = 'set-talks-error';
export const SET_TALKS_SCHEDULE_DATE = 'set-talk-schedule-date';
export const GENERATE_SCHEDULE = 'set-talks-schedule';
