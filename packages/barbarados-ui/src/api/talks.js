import getHeaders from './get-headers';
import throwErrorIfNotOk from './throw-error-if-not-ok';

const URL = 'http://localhost:3001/development';

export async function getTalk(token, talkId) {
  const response = await fetch(`${URL}/talks/${talkId}`, {
    method: 'GET',
    headers: getHeaders({ token })
  });

  const json = await response.json();
  throwErrorIfNotOk(json);
  return json;
}

export async function updateTalk(token, talk) {
  const response = await fetch(`${URL}/talks/${talk.talkId}`, {
    method: 'PUT',
    headers: getHeaders({ token }),
    body: JSON.stringify(talk)
  });

  const json = await response.json();
  throwErrorIfNotOk(json);
  return json;
}

export async function getAllTalks(token) {
  const response = await fetch(`${URL}/talks`, {
    method: 'GET',
    headers: getHeaders({ token })
  });

  const json = await response.json();
  throwErrorIfNotOk(json);
  return json;
}

export async function addStarToTalk(token, talkId) {
  const response = await fetch(`${URL}/talks/${talkId}/stars`, {
    method: 'POST',
    headers: getHeaders({ token })
  });

  const json = await response.json();
  throwErrorIfNotOk(json);
  return json;
}

export async function deleteStarFromTalk(token, talkId) {
  const response = await fetch(`${URL}/talks/${talkId}/stars`, {
    method: 'DELETE',
    headers: getHeaders({ token })
  });

  if (response.status !== 204) {
    const json = await response.json();
    throwErrorIfNotOk(json);
    return json;
  }

  return response;
}
