import getHeaders from './get-headers';
import throwErrorIfNotOk from './throw-error-if-not-ok';

const URL = 'http://localhost:3000/development';

export async function authenticate(email) {
  const response = await fetch(`${URL}/users/authenticate`, {
    method: 'POST',
    headers: getHeaders(),
    body: JSON.stringify({ email })
  });

  return await response.json();
}

export async function getUserBy({ email, token }) {
  const response = await fetch(`${URL}/users/${email}`, {
    method: 'GET',
    headers: getHeaders({ token })
  });

  const json = await response.json();

  throwErrorIfNotOk(json);
  return json;
}
