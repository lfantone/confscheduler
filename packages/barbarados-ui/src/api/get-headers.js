import { compose, concat, evolve, always, ifElse } from 'ramda';
import { rejectNil, isNilOrEmpty } from '@flybondi/ramda-land';

const PREFIX = 'development/';
const concatPrefix = concat(PREFIX);
const addPrefixOrNull = ifElse(isNilOrEmpty, always(null), concatPrefix);
const evolveHeaders = compose(rejectNil, evolve({ Authorization: addPrefixOrNull }));

export default function getHeaders({ token, additionalHeaders } = {}) {
  const headers = evolveHeaders({
    ...additionalHeaders,
    'Content-Type': 'application/json',
    Authorization: token
  });

  return headers;
}
