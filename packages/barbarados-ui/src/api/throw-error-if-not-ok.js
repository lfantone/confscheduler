import { when, compose, lte, prop } from 'ramda';

export default function throwErrorIfNotOk({ message, statusCode, status }) {
  return when(
    compose(lte(400), prop('statusCode')),
    () => {
      throw new Error(message);
    },
    {
      statusCode: statusCode ?? status
    }
  );
}
