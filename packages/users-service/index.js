'use strict';
const createAuthenticateUserHandler = require('./src/authenticate-user/authenticate-user-handler');
const createCreateUserHandler = require('./src/create-user/create-user-handler');
const createRetrieveUserHandler = require('./src/retrieve-user/retrieve-user-handler');
const { createOfflineAuthoriser } = require('@confscheduler/toolbox');

const { AWS_XRAY_DAEMON_ADDRESS } = process.env;
// Enable tracing if configuration allows for it
if (AWS_XRAY_DAEMON_ADDRESS) {
  require('aws-xray-sdk').captureHTTPsGlobal(require('https'), true);
}

const CONFIG = {
  logger: 'debug',
  table: 'development-conference-scheduler',
  token: {
    secret: 'SUPER TOKEN SECRET',
    expiresIn: '6 h',
    issuer: '@confscheduler/users-service',
    audience: 'web'
  }
};

module.exports = {
  authenticateUserHandler: createAuthenticateUserHandler(CONFIG),
  createUserHandler: createCreateUserHandler(CONFIG),
  retrieveUserHandler: createRetrieveUserHandler(CONFIG),
  offlineAuthoriser: createOfflineAuthoriser(CONFIG)
};
