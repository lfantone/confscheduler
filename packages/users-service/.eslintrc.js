'use strict';

module.exports = {
  extends: ['../../eslint.config.js', 'plugin:jsdoc/recommended'],
  plugins: ['jsdoc'],
  settings: {
    jsdoc: {
      mode: 'typescript'
    }
  },
  root: true
};
