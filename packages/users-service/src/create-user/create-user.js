'use strict';

const { CREATED, CONFLICT } = require('http-status-codes');
const { isNilOrEmpty, rejectNilOrEmpty } = require('@flybondi/ramda-land');
const { getUsersCollectionId, createUsersCollectionKey } = require('@confscheduler/toolbox');
const { applySpec, equals, compose, prop, ifElse, always, toLower } = require('ramda');

/**
 * @typedef User
 * @type {object}
 * @property {string} id The user collection id.
 * @property {string} key The user collection key.
 * @property {string} createdAt An ISO Date string when the candidate was created.
 * @property {string} email The user email.
 *
 */

/**
 * Parse given HTTP Body object into a user object.
 *
 * @param {import('./create-user-handler').Body} body HTTP Request parsed body.
 * @returns {User} a new user object.
 */
function applyUserSpec(body) {
  return compose(
    rejectNilOrEmpty,
    applySpec({
      id: prop('id'),
      key: prop('key'),
      admin: ifElse(compose(equals('admin'), toLower, prop('type')), always(true), always(null)),
      createdAt: () => new Date().toISOString(),
      email: prop('email'),
      image: compose(seed => `https://api.adorable.io/avatars/175/${seed}`, prop('email')),
      name: prop('name'),
      speaker: ifElse(compose(equals('speaker'), toLower, prop('type')), always(true), always(null))
    })
  )(body);
}

/**
 * Creates a new user entry in the DynamoDB table.
 * Before inserting a new record in the dB, it will check if the user already exist.
 * If the `userId` exists, it won't add a new record to the table and would response with a HTTP 409.
 * Otherwise, response will be HTTP 201 with the email and userId as a JSON response.
 *
 * @param {import('./create-user-handler').AWSAPIGatewayEvent} event an AWS API Gateway event
 * @returns {Promise<import('./create-user-handler').AWSAPIGatewayResponse>} API Gateway handler response
 */
async function createUser(event) {
  const {
    body,
    requestContext: {
      flynamo: { insert, get },
      console
    }
  } = event;

  console.log('[create-user] Received request with', body);

  const { email } = body;
  const id = getUsersCollectionId();
  const key = createUsersCollectionKey(email);

  console.debug(`[create-user] Checking if user with id ${id} and key ${key}`);

  const user = await get({ id, key });
  console.debug('[create-user] User fetched from dB', user);

  if (isNilOrEmpty(user)) {
    const newUser = applyUserSpec({ ...body, id, key });

    console.log(`[create-user] Creating a new user in ${id} with key ${key}`);
    console.debug('[create-user] User payload is', newUser);

    await insert(newUser);

    return {
      statusCode: CREATED,
      body: JSON.stringify(newUser)
    };
  }

  return {
    statusCode: CONFLICT,
    body: JSON.stringify({ errors: ['User already exist'] })
  };
}

module.exports = createUser;
