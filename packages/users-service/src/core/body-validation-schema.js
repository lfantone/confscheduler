'use strict';
const yup = require('yup');

const TYPES = ['user', 'speaker', 'admin'];

const userSchema = yup.object().shape({
  email: yup.string().trim().email().required()
});

const createUserSchema = yup.object().shape({
  email: yup.string().trim().email().required(),
  name: yup.string().trim().required(),
  type: yup.string().oneOf(TYPES).required()
});

module.exports = { createUserSchema, userSchema };
