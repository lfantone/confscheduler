'use strict';
const { OK } = require('http-status-codes');
const {
  createUsersCollectionKey,
  getUsersCollectionId,
  throwErrorIfResourceIsNilOrEmpty
} = require('@confscheduler/toolbox');
const { compose, pluck, pick } = require('ramda');
const { rejectNilOrEmpty } = require('@flybondi/ramda-land');

const getTalkIdsFrom = pluck('talkId');
const parseUser = compose(
  rejectNilOrEmpty,
  pick(['createdAt', 'image', 'name', 'email', 'admin', 'speaker'])
);
/**
 * Retrieve a user entry by the given path parameter from the DynamoDB table.
 *
 * @param {import('./retrieve-user-handler').AWSAPIGatewayEvent} event an AWS API Gateway event
 * @returns {Promise<import('./retrieve-user-handler').AWSAPIGatewayResponse>} API Gateway handler response
 */
async function retrieveUser(event) {
  const {
    pathParameters: { email },
    requestContext: {
      console,
      flynamo: { get, query }
    }
  } = event;
  const id = getUsersCollectionId();
  const key = createUsersCollectionKey(email);
  const params = {
    IndexName: 'user-stars-index',
    KeyConditionExpression: '#user = :user',
    ExpressionAttributeNames: {
      '#user': 'user'
    },
    ExpressionAttributeValues: {
      ':user': { S: email }
    }
  };

  console.log(`[retrieve-user] Received request to get a user with id ${id} and key ${key}`);

  const user = await get({ id, key });
  console.debug('[retrieve-user] User retrieved.', user);

  throwErrorIfResourceIsNilOrEmpty(user);

  console.debug('[retrieve-user] Getting all stars', params);
  const stars = await query(params);

  return {
    statusCode: OK,
    body: JSON.stringify({ ...parseUser(user), stars: getTalkIdsFrom(stars) })
  };
}

module.exports = retrieveUser;
