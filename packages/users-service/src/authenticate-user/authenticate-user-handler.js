'use strict';
const { compose } = require('ramda');
const { isNilOrEmpty } = require('@flybondi/ramda-land');
const {
  createEventRequestContextInjector,
  createFlynamoConnectorFor,
  createJWTFor,
  createLoggerFor,
  parseBody,
  tryCatch,
  yupValidation
} = require('@confscheduler/toolbox');
const authenticateUser = require('./authenticate-user');
const { userSchema } = require('../core');

/**
 *
 * @typedef {object} RequestContext
 * @property {import('console')} console a native console proxy wrapper to enable/disable debug messages.
 * @property {import('@confscheduler/toolbox/src/connectors/create-flynamo-connector').FlynamoAPI} flynamo the initialised Flynamo client.
 * @property {import('@confscheduler/toolbox/src/connectors/create-jwt-connector').JWTAPI} jwt the JWT API.
 */

/**
 * AWS API Gateway response object.
 *
 * @typedef {object} AWSAPIGatewayResponse
 * @property {number} statusCode HTTP Status code.
 * @property {string} body Handler response.
 */

/**
 * AWS API Gateway event.
 *
 * @typedef {object} AWSAPIGatewayEvent
 * @property {object} headers Incoming http request headers.
 * @property {RequestContext} requestContext Request global context.
 * @property {string} path Request URL path.
 * @property {string} httpMethod HTTP Request method.
 * @property {string} queryStringParameters HTTP Request query string parameters.
 * @property {object} body HTTP Request body.
 * @property {string} body.email The user email string value.
 */

/**
 * Builds AWS λ handler functions from the given `config` and injects required dependencies into their contexts.
 *
 * @param {object} config The service configuration object.
 * @param {string} config.logger The logger level value.
 * @param {string} config.table The platform DynamoDB table.
 * @param {object} config.token The JWT config values.
 * @param {string} config.token.secret a value used to generate the symmetric key.
 * @param {string} config.token.expiresIn a JWT Expiration Time to when creating a new token.
 * @param {string} config.token.audience a JWT Audience value used when verifying/creating a token.
 * @param {string} config.token.issuer a JWT Issuer value used when creating a token.
 * @returns {authenticateUser} The AWS Lambda handler.
 */
function createAuthenticateUserHandler(config) {
  const withLogger = createEventRequestContextInjector(() => createLoggerFor(config.logger));
  const withFlynamo = createEventRequestContextInjector(() =>
    createFlynamoConnectorFor(config.table)
  );
  const withJWT = createEventRequestContextInjector(() => createJWTFor(config.token));

  return compose(
    tryCatch((res, err, statusCode, event) => {
      const {
        requestContext: { requestId },
        body
      } = event;

      console.error(
        `[authenticate-user][${statusCode}] An error occurred while processing the request to authenticate a user: ${err.message}`
      );
      console.debug(`[authenticate-user] Request (${requestId})`, body, err);
    }),
    parseBody,
    yupValidation(userSchema, (res, err, statusCode, event) => {
      console.error(
        `[authenticate-user] [${statusCode}] ${
          isNilOrEmpty(err.errors)
            ? `An unexpected error occurred: ${err.message}`
            : `Invalid request or missing arguments: ${JSON.stringify(err.errors)}`
        }`
      );
    }),
    withFlynamo(forTable => [forTable, { as: 'flynamo' }]),
    withLogger(console => [console, { as: 'console' }]),
    withJWT(jwt => [jwt, { as: 'jwt' }])
  )(authenticateUser);
}

module.exports = createAuthenticateUserHandler;
