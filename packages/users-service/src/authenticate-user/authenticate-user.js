'use strict';

const { BAD_REQUEST, OK } = require('http-status-codes');
const { createUsersCollectionKey, getUsersCollectionId } = require('@confscheduler/toolbox');
const { isNotNilOrEmpty } = require('@flybondi/ramda-land');
const { pick } = require('ramda');

const pickAttributesFrom = pick(['email', 'admin', 'speaker']);

/**
 * Authenticate given user by checking the given password against the one stored in the database and
 * if they match It will create a new JWT token, update the user with the new fresh token and response it back to the requester.
 *
 * @param {import('./authenticate-user-handler').AWSAPIGatewayEvent} event an AWS API Gateway event
 * @returns {Promise<import('./authenticate-user-handler').AWSAPIGatewayResponse>} API Gateway handler response
 */
async function authenticateUser(event) {
  const {
    body,
    requestContext: {
      flynamo: { get, update },
      console,
      jwt: { sign, decode }
    }
  } = event;
  const { email } = body;
  const id = getUsersCollectionId();
  const key = createUsersCollectionKey(email);

  console.log('[authenticate-user] Received request with', body);
  console.debug(`[authenticate-user] Checking if user with id ${id} and key ${key} already exist`);

  const user = await get({ id, key });
  console.debug(`[authenticate-user] User fetched from dB`, user);

  if (isNotNilOrEmpty(user)) {
    console.log('[authticate-user] Creating a new user token');
    const token = sign(pickAttributesFrom(user));
    const payload = decode(token);

    console.debug('[authenticate-user] Token decoded:', payload);
    console.debug(
      `[authenticate-user] Updating user with id ${id} and key ${key} with new session`
    );
    console.log('[authenticate-user] Inserting user token in dB');

    await update({ id, key }, { session: token, ttl: payload.exp });

    return {
      statusCode: OK,
      body: JSON.stringify({ token })
    };
  }

  console.log('[authenticate-user] The service was not able to authenticate given user');
  return {
    statusCode: BAD_REQUEST,
    body: JSON.stringify({ errors: ['Invalid user email or password'] })
  };
}

module.exports = authenticateUser;
