'use strict';

const createTokenAuthoriserHandler = require('./src/token-authoriser/token-authoriser-handler');

const { AWS_XRAY_DAEMON_ADDRESS } = process.env;
// Enable tracing if configuration allows for it
if (AWS_XRAY_DAEMON_ADDRESS) {
  require('aws-xray-sdk').captureHTTPsGlobal(require('https'));
}
const CONFIG = {
  logger: 'debug',
  allowedResource: 'arn:aws:execute-api:us-west-1:random-account-id:*/',
  token: {
    secret: 'SUPER TOKEN SECRET',
    expiresIn: '30 m',
    issuer: '@recruitment-platform/users-service',
    audience: 'web'
  }
};

module.exports = {
  tokenAuthoriserHandler: createTokenAuthoriserHandler(CONFIG),
  test: async function handleEvent(event) {
    return {
      statusCode: 200,
      body: JSON.stringify({ ok: true })
    };
  }
};
