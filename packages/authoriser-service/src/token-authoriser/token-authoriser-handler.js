'use strict';

const { compose } = require('ramda');
const {
  createEventRequestContextInjector,
  createLoggerFor,
  createJWTFor
} = require('@confscheduler/toolbox');
const tokenAuthoriser = require('./token-authoriser');
const createGeneratePolicyFor = require('./generate-policy');

/**
 *
 * @typedef {object} AuthContext
 * @property {(token: string) => import('@confscheduler/toolbox/src/connectors/create-jwt-connector').DecryptedToken} verify the JWT.verify method.
 * @property {typeof createGeneratePolicyFor} generatePolicy Builds and returns a policy document based on the given parameters.
 */

/**
 *
 * @typedef {object} RequestContext
 * @property {import('console')} console a native console proxy wrapper to enable/disable debug messages.
 */

/**
 * Builds AWS λ handler functions from the given `config` and injects required dependencies into their contexts.
 *
 * @param {object} config The service configuration object.
 * @param {string} config.logger The logger level value.
 * @param {string} config.allowedResource The service allowed resorces.
 * @param {object} config.token The JWT config values.
 * @param {string} config.token.secret a value used to generate the symmetric key.
 * @param {string} config.token.expiresIn a JWT Expiration Time to when creating a new token.
 * @param {string} config.token.audience a JWT Audience value used when verifying/creating a token.
 * @param {string} config.token.issuer a JWT Issuer value used when creating a token.
 * @returns {tokenAuthoriser} The AWS Lambda handler.
 */
function createAuthoriserEventHandler(config) {
  const withLogger = createEventRequestContextInjector(() => createLoggerFor(config.logger));
  const withJWT = createEventRequestContextInjector(() => createJWTFor(config.token), {
    context: 'authoriseContext'
  });
  const withPolicyGenerator = createEventRequestContextInjector(
    () => createGeneratePolicyFor(config.allowedResource),
    {
      context: 'authoriseContext'
    }
  );

  return compose(
    withLogger(console => [console, { as: 'console' }]),
    withJWT(({ verify }) => [verify, { as: 'verify' }]),
    withPolicyGenerator(generatePolicy => [generatePolicy, { as: 'generatePolicy' }])
  )(tokenAuthoriser);
}

module.exports = createAuthoriserEventHandler;
