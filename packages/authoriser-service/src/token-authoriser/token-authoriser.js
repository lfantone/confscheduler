'use strict';
const { isNotNilOrEmpty } = require('@flybondi/ramda-land');

/**
 * AWS API Authorizer response object.
 *
 * @typedef {object} AWSAPIAuthoriserResponse
 * @property {number} statusCode HTTP Status code.
 * @property {string} body Handler response.
 */

/**
 * AWS API Gateway event.
 *
 * @typedef {object} AWSAPIAuthoriserEvent
 * @property {string} authorizationToken Incoming http request headers.
 * @property {import('./token-authoriser-handler').AuthContext} authoriseContext Auth global context.
 * @property {import('./token-authoriser-handler').RequestContext} requestContext Request global context.
 */

/**
 * Main AWS λ handler. Lambda token authorizer for APIte keys. Allows or denies access to resources
 * based on a JWT encrypted token.
 *
 * @param {AWSAPIAuthoriserEvent} event an AWS API Gateway event
 * @returns {AWSAPIAuthoriserResponse} API Gateway handler response
 */
async function authenticateUser(event) {
  const {
    authorizationToken,
    authoriseContext: { verify, generatePolicy },
    requestContext: { console }
  } = event;

  console.log(`[token-authoriser] Received request to authorise with ${authorizationToken}`);

  try {
    const { email, aud, iss, admin, speaker, scope } = verify(authorizationToken);
    console.log(`[token-authoriser] Generating request access policy to ${email}`);

    return generatePolicy({
      principalId: `${email}:${aud}`,
      effect: 'Allow',
      // If the provided token grants permissions for a particular path (or set of paths),
      // grant access to the ARN being requested, otherwise leave unset to generate a
      // default Allow policy using the value of `ALLOWED_RESOURCE` environment variable
      resource: isNotNilOrEmpty(scope) ? event.methodArn : undefined,
      context: {
        clientAudience: aud,
        clientSource: iss,
        user: { email, admin, speaker },
        clientToken: authorizationToken
      }
    });
  } catch (error) {
    console.log(`[token-authoriser] The request was denied for token ${authorizationToken}`);
    console.debug(`[token-authoriser] Error`, error);
    return generatePolicy({
      principalId: 'anonymous',
      effect: 'Deny'
    });
  }
}

module.exports = authenticateUser;
