'use strict';

const { curry } = require('@flybondi/ramda-land');

/**
 * Policy document action statement.
 *
 * @type {string}
 */
const POLICY_ACTION = 'execute-api:Invoke';

/**
 * Policy document version.
 *
 * @type {string}
 */
const POLICY_VERSION = '2012-10-17';

/**
 * @typedef {object} Attrs Policy attributes.
 * @property {string} principalId Principal identifier.
 * @property {string=} resource The resource affected by this policy.
 * @property {string} effect Either 'Allow' or 'Deny'.
 * @property {object} context The policy context.
 */

/**
 * @typedef {object} Statement
 * @property {string} Action The policy statement action.
 * @property {string} Effect the policy statement effect.
 * @property {Array<string>} Resource The policy statement resource.
 */

/**
 * @typedef {object} Policy
 * @property {string} principalId The policy principal id.
 * @property {object} context The policy context.
 * @property {object} policyDocument The policy document.
 * @property {Array<Statement>} policyDocument.Statement The policy document statement array.
 * @property {string} policyDocument.Version The polocy document version.
 */

/**
 * Builds and returns a policy document based on the given parameters.
 *
 * @see https://docs.aws.amazon.com/apigateway/latest/developerguide/apigateway-use-lambda-authorizer.html
 * @param {string} defaultResource The resource affected by this policy - used only if
 *  `attrs.resource` is not specified.
 * @param {Attrs} attrs Policy attributes.
 * @returns {Policy} An IAM policy descriptor.
 */
function generatePolicy(defaultResource, { principalId, resource, effect, context = {} }) {
  return {
    principalId,
    context,
    policyDocument: {
      Statement: [
        {
          Action: POLICY_ACTION,
          Effect: effect,
          Resource: [resource || defaultResource]
        }
      ],
      Version: POLICY_VERSION
    }
  };
}

/**
 * Inject default resource to the generatePolicy function.
 *
 * @param {string} resource The resource affected by this policy.
 * @returns {(attrs: Attrs) => Policy} The generator function
 */
function createGeneratePolicyFor(resource) {
  return curry(generatePolicy)(resource);
}

module.exports = createGeneratePolicyFor;
