'use strict';
const createCreateRoomHandler = require('./src/create-room/create-room-handler');
const createRetrieveAllRoomsHandler = require('./src/retrieve-all-rooms/retrieve-all-rooms-handler');
const createRetrieveRoomHandler = require('./src/retrieve-room/retrieve-room-handler');
const { createOfflineAuthoriser } = require('@confscheduler/toolbox');

const { AWS_XRAY_DAEMON_ADDRESS } = process.env;
// Enable tracing if configuration allows for it
if (AWS_XRAY_DAEMON_ADDRESS) {
  require('aws-xray-sdk').captureHTTPsGlobal(require('https'), true);
}

const CONFIG = {
  logger: 'debug',
  table: 'development-conference-scheduler',
  token: {
    secret: 'SUPER TOKEN SECRET',
    audience: ['web', 'development']
  }
};

module.exports = {
  createRoomHandler: createCreateRoomHandler(CONFIG),
  retrieveAllRoomsHandler: createRetrieveAllRoomsHandler(CONFIG),
  retrieveRoomHandler: createRetrieveRoomHandler(CONFIG),
  offlineAuthoriser: createOfflineAuthoriser(CONFIG)
};
