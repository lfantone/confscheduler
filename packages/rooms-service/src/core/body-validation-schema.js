'use strict';
const yup = require('yup');
const isUUID = require('is-uuid');
const { ifElse, always } = require('ramda');
const { isNotNilOrEmpty } = require('@flybondi/ramda-land');

const runValidationIfNotNil = validation => ifElse(isNotNilOrEmpty, validation, always(true));

const roomSchema = yup.object().shape({
  description: yup.string(),
  talkId: yup.string().test('is-uuid-v5', runValidationIfNotNil(isUUID.v5)).nullable(),
  title: yup.string().required()
});

const roomIdSchema = yup.object().shape({
  roomId: yup.string().test('is-uuid-v5', isUUID.v5).required()
});

module.exports = { roomSchema, roomIdSchema };
