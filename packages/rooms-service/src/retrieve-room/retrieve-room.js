'use strict';
const { OK } = require('http-status-codes');
const {
  createRoomsCollectionKey,
  getRoomsCollectionId,
  throwErrorIfResourceIsNilOrEmpty
} = require('@confscheduler/toolbox');

/**
 * Retrieve a conference room entry by the given path parameter from the DynamoDB table.
 *
 * @param {import('./retrieve-room-handler').AWSAPIGatewayEvent} event an AWS API Gateway event
 * @returns {Promise<import('./retrieve-room-handler').AWSAPIGatewayResponse>} API Gateway handler response
 */
async function retrieveTalk(event) {
  const {
    pathParameters: { roomId },
    requestContext: {
      console,
      flynamo: { get }
    }
  } = event;
  const id = getRoomsCollectionId();
  const key = createRoomsCollectionKey(roomId);

  console.log(
    `[retrieve-room] Received request to get a conference room with id ${id} and key ${key}`
  );

  const room = await get({ id, key });
  console.debug('[retrieve-room] Conference room retrieved.', room);

  throwErrorIfResourceIsNilOrEmpty(room);

  return {
    statusCode: OK,
    body: JSON.stringify(room)
  };
}

module.exports = retrieveTalk;
