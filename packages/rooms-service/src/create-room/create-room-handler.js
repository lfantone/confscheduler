'use strict';
const { compose } = require('ramda');
const { isNilOrEmpty } = require('@flybondi/ramda-land');
const {
  createEventRequestContextInjector,
  createFlynamoConnectorFor,
  createLoggerFor,
  parseBody,
  yupValidation,
  tryCatch
} = require('@confscheduler/toolbox');
const createRoom = require('./create-room');
const { roomSchema } = require('../core');

/**
 * Request body
 *
 * @typedef {object} Body
 * @property {string} name The conference room name.
 * @property {string} [description] The conference room descriptor.
 */

/**
 * @typedef {object} AuthoriserContext
 * @property {object} user User information object.
 * @property {boolean} [user.admin] Requester admin flag.
 * @property {boolean} [user.speaker] Request speaker flag.
 * @property {string} user.email Requester user email.
 */

/**
 *
 * @typedef {object} RequestContext
 * @property {import('console')} console a native console proxy wrapper to enable/disable debug messages.
 * @property {import('@confscheduler/toolbox/src/connectors/create-flynamo-connector').FlynamoAPI} flynamo the initialised Flynamo client.
 * @property {AuthoriserContext} authorizer Request authoriser generated context.
 */

/**
 * AWS API Gateway response object.
 *
 * @typedef {object} AWSAPIGatewayResponse
 * @property {number} statusCode HTTP Status code.
 * @property {string} body Handler response.
 */

/**
 * AWS API Gateway event.
 *
 * @typedef {object} AWSAPIGatewayEvent
 * @property {object} headers Incoming http request headers.
 * @property {RequestContext} requestContext Request global context.
 * @property {string} path Request URL path.
 * @property {string} httpMethod HTTP Request method.
 * @property {string} queryStringParameters HTTP Request query string parameters.
 * @property {Body} body HTTP Request body.
 */

/**
 * Builds AWS λ handler functions from the given `config` and injects required dependencies into their contexts.
 *
 * @param {object} config The service configuration object.
 * @param {string} config.logger The logger level value.
 * @param {string} config.table The platform table name
 * @returns {createRoom} The AWS Lambda handler.
 */
function createCreateRoomHandler(config) {
  const withLogger = createEventRequestContextInjector(() => createLoggerFor(config.logger));
  const withFlynamo = createEventRequestContextInjector(() =>
    createFlynamoConnectorFor(config.table)
  );

  return compose(
    tryCatch((res, err, statusCode, event) => {
      const {
        requestContext: { requestId },
        headers
      } = event;
      const token = headers.Authorization;

      console.error(
        `[create-room][${statusCode}] An error occurred while processing the request to create a new conference talk: ${err.message}`
      );
      console.debug(`[create-room] Request (${requestId})`, token, err);
    }),
    parseBody,
    yupValidation(roomSchema, (res, err, statusCode, event) => {
      console.error(
        `[create-room] [${statusCode}] ${
          isNilOrEmpty(err.errors)
            ? `An unexpected error occurred: ${err.message}`
            : `Invalid request or missing arguments: ${JSON.stringify(err.errors)}`
        }`
      );
    }),
    withFlynamo(forTable => [forTable, { as: 'flynamo' }]),
    withLogger(console => [console, { as: 'console' }])
  )(createRoom);
}

module.exports = createCreateRoomHandler;
