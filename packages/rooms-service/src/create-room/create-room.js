'use strict';

const { CONFLICT, CREATED, FORBIDDEN } = require('http-status-codes');
const { isNilOrEmpty, rejectNilOrEmpty } = require('@flybondi/ramda-land');
const {
  getRoomsCollectionId,
  createRoomsCollectionKey,
  throwErrorIf
} = require('@confscheduler/toolbox');
const { applySpec, compose, prop, flip, curryN, converge, concat, when, always } = require('ramda');
const { v5 } = require('uuid');

const NAMESPACE = '4b699b32-7e87-4be0-99a4-1eeb5d41a94f';

/**
 * @typedef User
 * @type {object}
 * @property {string} email The user email
 * @property {boolean} [admin] True when the user is an admin.
 * @property {boolean} [speaker] True when the user is an speaker.
 *
 */

/**
 * @typedef Talk
 * @type {object}
 * @property {string} id The talk collection id.
 * @property {string} key The talk collection key.
 * @property {string} createdAt An ISO Date string when the candidate was created.
 * @property {string} speaker The conference talk speaker email.
 * @property {string} room The conference talk room uuid.
 * @property {string} time The conference talk start time in ISO string format.
 *
 */

/**
 * Throws an error unless the `user.admin` property is not nil or empty in the context.
 *
 * @example
 *  throwErrorIfUserIsNotAdmin({ admin: null })  // --> throws FORBIDDEN error
 *  throwErrorIfUserIsNotAdmin({ admin: true })  // --> nothing!
 *
 * @function
 * @type {(user: User) => void}
 * @throws {Error}
 */
const throwErrorIfUserIsNotAdmin = throwErrorIf(
  'User is not an admin. Only admins are allowed to create a conference talk',
  'admin',
  FORBIDDEN
);

const uuidWithNamespace = flip(curryN(2, v5))(NAMESPACE);
const dateNowString = compose(toString, Date.now);
const concatTitleWithDate = converge(concat, [prop('title'), dateNowString]);

/**
 * Generates an uuid string with the `title` and Date.now value.
 *
 * @example generateIdFromUserAndDate({ title: 'foo' }) // -> 'uuid'
 * @param {object} obj the function main argument.
 * @param {string} obj.title the conference talk title.
 * @returns {string} the generated id
 */
function generateIdFromTitle(obj) {
  return compose(uuidWithNamespace, concatTitleWithDate)(obj);
}

/**
 * Parse given HTTP Body object into a user object.
 *
 * @param {import('./create-room-handler').Body} body HTTP Request parsed body.
 * @returns {Talk} a new user object.
 */
function applyRoomSpec(body) {
  return compose(
    rejectNilOrEmpty,
    applySpec({
      id: prop('id'),
      key: prop('key'),
      createdAt: () => new Date().toISOString(),
      title: prop('title'),
      description: prop('description'),
      creator: prop('email'),
      talkId: compose(when(isNilOrEmpty, always(null)), prop('talkId')),
      roomId: prop('roomId'),
      image: compose(seed => `https://picsum.photos/seed/${seed}/200/300?greyscale`, prop('roomId'))
    })
  )(body);
}

/**
 * Creates a new conference talk entry in the DynamoDB table.
 * Before inserting a new record in the dB, it will check if the user already exist.
 * If the `talk` exists, it won't add a new record to the table and would response with a HTTP 409.
 * Otherwise, response will be HTTP 201 with recently created conference talk as a JSON response.
 *
 * @param {import('./create-room-handler').AWSAPIGatewayEvent} event an AWS API Gateway event
 * @returns {Promise<import('./create-room-handler').AWSAPIGatewayResponse>} API Gateway handler response
 */
async function createTalk(event) {
  const {
    body,
    requestContext: {
      authorizer: { user },
      flynamo: { insert, get },
      console
    }
  } = event;

  console.log('[create-room] Received request with', body);

  throwErrorIfUserIsNotAdmin(user);

  const roomId = generateIdFromTitle({ title: body.title });
  const id = getRoomsCollectionId();
  const key = createRoomsCollectionKey(roomId);

  console.debug(`[create-room] Checking if room with id ${id} and key ${key} already exist`);

  const room = await get({ id, key });
  console.debug('[create-room] Conference room fetched from dB', room);

  if (isNilOrEmpty(room)) {
    const newRoom = applyRoomSpec({ ...body, id, key, roomId });

    console.log(`[create-room] Creating a new conference room in ${id} with key ${key}`);
    console.debug('[create-room] Conference room payload is', newRoom);

    await insert(newRoom);

    return {
      statusCode: CREATED,
      body: JSON.stringify(newRoom)
    };
  }

  return {
    statusCode: CONFLICT,
    body: JSON.stringify({ errors: ['Conference talk already exist'] })
  };
}

module.exports = createTalk;
