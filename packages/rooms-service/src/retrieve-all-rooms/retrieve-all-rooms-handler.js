'use strict';

const { compose } = require('ramda');
const {
  createEventRequestContextInjector,
  createFlynamoConnectorFor,
  createLoggerFor,
  tryCatch
} = require('@confscheduler/toolbox');
const retrieveAllRooms = require('./retrieve-all-rooms');

/**
 *
 * @typedef {object} RequestContext
 * @property {import('console')} console a native console proxy wrapper to enable/disable debug messages.
 * @property {import('@confscheduler/toolbox/src/connectors/create-flynamo-connector').FlynamoAPI} flynamo the initialised Flynamo client.
 */

/**
 * AWS API Gateway response object.
 *
 * @typedef {object} AWSAPIGatewayResponse
 * @property {number} statusCode HTTP Status code.
 * @property {string} body Handler response.
 */

/**
 * AWS API Gateway event.
 *
 * @typedef {object} AWSAPIGatewayEvent
 * @property {object} headers Incoming http request headers.
 * @property {RequestContext} requestContext Request global context.
 * @property {string} path Request URL path.
 * @property {object} queryStringParameters HTTP Request query string parameters.
 * @property {boolean} [queryStringParameters.available=false] Boolean parameter that will make to return only available rooms only. Default to `false`.
 * @property {string} httpMethod HTTP Request method.
 */

/**
 * Builds AWS λ handler functions from the given `config` and injects required dependencies into their contexts.
 *
 * @param {object} config The service configuration object.
 * @param {string} config.logger The logger level value.
 * @param {string} config.table The platform table name
 * @returns {retrieveAllRooms} The AWS Lambda handler.
 */
function createRetrieveAllRoomsHandler(config) {
  const withLogger = createEventRequestContextInjector(() => createLoggerFor(config.logger));
  const withFlynamo = createEventRequestContextInjector(() =>
    createFlynamoConnectorFor(config.table)
  );

  return compose(
    tryCatch((res, err, statusCode, event) => {
      const {
        requestContext: { requestId },
        headers
      } = event;
      const token = headers.Authorization;

      console.error(
        `[retrieve-all-rooms][${statusCode}] An error occurred while processing the request to retrieve all conference rooms: ${err.message}`
      );
      console.debug(`[retrieve-all-rooms] Request (${requestId})`, token, err);
    }),
    withFlynamo(forTable => [forTable, { as: 'flynamo' }]),
    withLogger(console => [console, { as: 'console' }])
  )(retrieveAllRooms);
}

module.exports = createRetrieveAllRoomsHandler;
