'use strict';
const { OK } = require('http-status-codes');
const { createRoomsCollectionKey, getRoomsCollectionId } = require('@confscheduler/toolbox');

/**
 * Retrieve a conference talk entry by the given path parameter from the DynamoDB table.
 *
 * @param {import('./retrieve-all-rooms-handler').AWSAPIGatewayEvent} event an AWS API Gateway event
 * @returns {Promise<import('./retrieve-all-rooms-handler').AWSAPIGatewayResponse>} API Gateway handler response
 */
async function retrieveAllRooms(event) {
  const {
    queryStringParameters: { available = false },
    requestContext: {
      console,
      flynamo: { query }
    }
  } = event;

  console.log('[retrieve-all-rooms] Received request to get all conference rooms');
  const params = {
    KeyConditionExpression: '#id = :id AND begins_with(#key, :key)',
    ExpressionAttributeNames: {
      '#id': 'id',
      '#key': 'key'
    },
    ExpressionAttributeValues: {
      ':id': { S: getRoomsCollectionId() },
      ':key': { S: createRoomsCollectionKey() }
    }
  };

  if (available) {
    const { ExpressionAttributeNames } = params;
    params.FilterExpression = 'attribute_not_exists(#talkId)';
    params.ExpressionAttributeNames = {
      ...ExpressionAttributeNames,
      '#talkId': 'talkId'
    };
  }

  const rooms = await query(params);
  console.debug('[retrieve-all-rooms] Conference rooms retrieved.', rooms);

  return {
    statusCode: OK,
    body: JSON.stringify(rooms)
  };
}

module.exports = retrieveAllRooms;
