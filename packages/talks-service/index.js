'use strict';

const createAddTalkStarHandler = require('./src/add-talk-star/add-talk-star-handler');
const createCreateTalkHandler = require('./src/create-talk/create-talk-handler');
const createRemoveTalkStarHandler = require('./src/remove-talk-star/remove-talk-star-handler');
const createRetrieveAllTalksHandler = require('./src/retrieve-all-talks/retrieve-all-talks-handler');
const createRetrieveTalkHandler = require('./src/retrieve-talk/retrieve-talk-handler');
const createUpdateTalkHandler = require('./src/update-talk/update-talk-handler');
const { createOfflineAuthoriser } = require('@confscheduler/toolbox');

const { AWS_XRAY_DAEMON_ADDRESS } = process.env;
// Enable tracing if configuration allows for it
if (AWS_XRAY_DAEMON_ADDRESS) {
  require('aws-xray-sdk').captureHTTPsGlobal(require('https'), true);
}

const CONFIG = {
  logger: 'debug',
  table: 'development-conference-scheduler',
  token: {
    secret: 'SUPER TOKEN SECRET',
    audience: ['web', 'development']
  }
};

module.exports = {
  addStarTalkHandler: createAddTalkStarHandler(CONFIG),
  createTalkHandler: createCreateTalkHandler(CONFIG),
  removeStarTalkHandler: createRemoveTalkStarHandler(CONFIG),
  retrieveAllTalksHandler: createRetrieveAllTalksHandler(CONFIG),
  retrieveTalkHandler: createRetrieveTalkHandler(CONFIG),
  updateTalkHandler: createUpdateTalkHandler(CONFIG),
  offlineAuthoriser: createOfflineAuthoriser(CONFIG)
};
