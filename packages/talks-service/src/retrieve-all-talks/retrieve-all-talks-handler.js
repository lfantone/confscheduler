'use strict';

const { compose } = require('ramda');
const {
  createEventRequestContextInjector,
  createFlynamoConnectorFor,
  createLoggerFor,
  tryCatch
} = require('@confscheduler/toolbox');
const retrieveAllTalks = require('./retrieve-all-talks');

/**
 * @typedef {object} AuthoriserContext
 * @property {object} user User information object.
 * @property {boolean} [user.admin] Requester admin flag.
 * @property {boolean} [user.speaker] Request speaker flag.
 * @property {string} user.email Requester user email.
 */

/**
 *
 * @typedef {object} RequestContext
 * @property {import('console')} console a native console proxy wrapper to enable/disable debug messages.
 * @property {import('@confscheduler/toolbox/src/connectors/create-flynamo-connector').FlynamoAPI} flynamo the initialised Flynamo client.
 * @property {AuthoriserContext} authorizer Request authoriser generated context.
 */

/**
 * AWS API Gateway response object.
 *
 * @typedef {object} AWSAPIGatewayResponse
 * @property {number} statusCode HTTP Status code.
 * @property {string} body Handler response.
 */

/**
 * AWS API Gateway event.
 *
 * @typedef {object} AWSAPIGatewayEvent
 * @property {object} headers Incoming http request headers.
 * @property {RequestContext} requestContext Request global context.
 * @property {string} path Request URL path.
 * @property {string} httpMethod HTTP Request method.
 */

/**
 * Builds AWS λ handler functions from the given `config` and injects required dependencies into their contexts.
 *
 * @param {object} config The service configuration object.
 * @param {string} config.logger The logger level value.
 * @param {string} config.table The platform table name
 * @returns {retrieveAllTalks} The AWS Lambda handler.
 */
function createRetrieveAllTalksHandler(config) {
  const withLogger = createEventRequestContextInjector(() => createLoggerFor(config.logger));
  const withFlynamo = createEventRequestContextInjector(() =>
    createFlynamoConnectorFor(config.table)
  );

  return compose(
    tryCatch((res, err, statusCode, event) => {
      const {
        requestContext: { requestId },
        headers
      } = event;
      const token = headers.Authorization;

      console.error(
        `[retrieve-all-talks][${statusCode}] An error occurred while processing the request to retrieve all conference talks: ${err.message}`
      );
      console.debug(`[retrieve-all-talks] Request (${requestId})`, token, err);
    }),
    withFlynamo(forTable => [forTable, { as: 'flynamo' }]),
    withLogger(console => [console, { as: 'console' }])
  )(retrieveAllTalks);
}

module.exports = createRetrieveAllTalksHandler;
