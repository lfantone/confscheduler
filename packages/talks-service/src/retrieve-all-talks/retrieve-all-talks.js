'use strict';
const { OK } = require('http-status-codes');
const { createTalksCollectionKey, getTalksCollectionId } = require('@confscheduler/toolbox');
const { prop, sortBy } = require('ramda');
const { retrieveStarCountFrom } = require('../core');

const sortByTime = sortBy(prop('time'));

/**
 * Retrieve a conference talk entry by the given path parameter from the DynamoDB table.
 *
 * @param {import('./retrieve-all-talks-handler').AWSAPIGatewayEvent} event an AWS API Gateway event
 * @returns {Promise<import('./retrieve-all-talks-handler').AWSAPIGatewayResponse>} API Gateway handler response
 */
async function retrieveAllTalks(event) {
  const {
    requestContext: {
      authorizer: { user },
      console,
      flynamo: { query }
    }
  } = event;

  console.log('[retrieve-all-talks] Received request to get all conference talks');

  const params = {
    KeyConditionExpression: '#id = :id AND begins_with(#key, :key)',
    ExpressionAttributeNames: {
      '#id': 'id',
      '#key': 'key'
    },
    ExpressionAttributeValues: {
      ':id': { S: getTalksCollectionId() },
      ':key': { S: createTalksCollectionKey() }
    }
  };

  let talks = await query(params);
  console.debug('[retrieve-all-talks] Conference talks retrieved.', talks);

  if (user.admin) {
    console.debug('[retrieve-all-talks] Retrieving all talks star count.');

    talks = await Promise.all(talks.map(talk => retrieveStarCountFrom(query, talk)));
  }

  if (user.speaker) {
    console.debug('[retrieve-all-talks] Retrieving all user starred talks count.');
    talks = await Promise.all(
      talks.map(talk => (talk.user === user.email ? retrieveStarCountFrom(query, talk) : talk))
    );
  }

  return {
    statusCode: OK,
    body: JSON.stringify(sortByTime(talks))
  };
}

module.exports = retrieveAllTalks;
