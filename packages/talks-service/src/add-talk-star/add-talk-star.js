'use strict';

const { CONFLICT, CREATED } = require('http-status-codes');
const { isNilOrEmpty, rejectNilOrEmpty } = require('@flybondi/ramda-land');
const {
  getTalksCollectionId,
  createTalksCollectionKey,
  createStarTalksCollectionKey,
  throwErrorIfResourceIsNilOrEmpty
} = require('@confscheduler/toolbox');
const { applySpec, compose, prop } = require('ramda');

/**
 * @typedef Star
 * @type {object}
 * @property {string} id The talk collection id.
 * @property {string} key The star collection key.
 * @property {string} createdAt An ISO Date string when the candidate was created.
 * @property {string} user The user email who starred the conference talk.
 * @property {string} talkId The conference talk uuid.
 *
 */

/**
 * Parse given object into an star object.
 *
 * @param {import('./create-talk-handler').Body} obj HTTP Request parsed body.
 * @returns {Star} a new user object.
 */
function applyStarSpec(obj) {
  return compose(
    rejectNilOrEmpty,
    applySpec({
      id: prop('id'),
      key: prop('starKey'),
      createdAt: () => new Date().toISOString(),
      user: prop('email'),
      talkId: prop('talkId')
    })
  )(obj);
}

/**
 * Adds an star to a conference talk.
 * If the conference talk was already starred by the user returns a 409.
 *
 * @param {import('./add-talk-star-handler').AWSAPIGatewayEvent} event an AWS API Gateway event
 * @returns {Promise<import('./add-talk-star-handler').AWSAPIGatewayResponse>} API Gateway handler response
 */
async function addTalkStar(event) {
  const {
    body,
    pathParameters: { talkId },
    requestContext: {
      authorizer: { user },
      flynamo: { insert, get },
      console
    }
  } = event;

  console.log('[add-talk-star] Received request with', body);

  const id = getTalksCollectionId();
  const key = createTalksCollectionKey(talkId);

  console.debug(`[add-talk-star] Checking if talk with id ${id} and key ${key} already exist`);

  const talk = await get({ id, key });
  console.debug('[add-talk-star] Conference talk fetched from dB', talk);

  throwErrorIfResourceIsNilOrEmpty(talk);

  const { email } = user;
  const starKey = createStarTalksCollectionKey(talkId, email);
  console.debug(
    `[add-talk-star] Checking if star with id ${id} and key ${starKey} is already exist for user ${email}`
  );
  const star = await get({ id, key: starKey });

  if (isNilOrEmpty(star)) {
    const newStar = applyStarSpec({ id, starKey, email, talkId });

    console.log(`[create-talk] Creating a new star ${id} with key ${starKey}`);
    console.debug('[create-talk] Star payload is', newStar);

    await insert(newStar);

    return {
      statusCode: CREATED,
      body: JSON.stringify(newStar)
    };
  }

  return {
    statusCode: CONFLICT,
    body: JSON.stringify({ errors: ['Talk already starred by user'] })
  };
}

module.exports = addTalkStar;
