'use strict';

const { CONFLICT, CREATED, FORBIDDEN } = require('http-status-codes');
const { isNilOrEmpty, rejectNilOrEmpty, isNotNilOrEmpty } = require('@flybondi/ramda-land');
const {
  getTalksCollectionId,
  createTalksCollectionKey,
  getUsersCollectionId,
  createUsersCollectionKey,
  throwErrorIf,
  throwErrorIfResourceIsNilOrEmpty
} = require('@confscheduler/toolbox');
const {
  applySpec,
  compose,
  concat,
  converge,
  curryN,
  flip,
  path,
  pick,
  prop,
  propOr
} = require('ramda');
const { parseISO } = require('date-fns/fp');
const { v5 } = require('uuid');

const NAMESPACE = '4b699b32-7e87-4be0-99a4-1eeb5d41a94f';

/**
 * @typedef User
 * @type {object}
 * @property {string} email The user email
 * @property {boolean} [admin] True when the user is an admin.
 * @property {boolean} [speaker] True when the user is an speaker.
 *
 */

/**
 * @typedef Talk
 * @type {object}
 * @property {string} id The talk collection id.
 * @property {string} key The talk collection key.
 * @property {string} createdAt An ISO Date string when the candidate was created.
 * @property {string} speaker The conference talk speaker email.
 * @property {string} room The conference talk room uuid.
 * @property {string} time The conference talk start time in ISO string format.
 *
 */

/**
 * Throws an error unless the `user.admin` property is not nil or empty in the context.
 *
 * @example
 *  throwErrorIfUserIsNotAdmin({ admin: null })  // --> throws FORBIDDEN error
 *  throwErrorIfUserIsNotAdmin({ admin: true })  // --> nothing!
 *
 * @function
 * @type {(user: User) => void}
 * @throws {Error}
 */
const throwErrorIfUserIsNotAdmin = throwErrorIf(
  'User is not an admin. Only admins are allowed to create a conference talk',
  'admin',
  FORBIDDEN
);

const uuidWithNamespace = flip(curryN(2, v5))(NAMESPACE);
const concatTitleWithEmail = converge(concat, [prop('title'), prop('email')]);

/**
 * Generates an uuid string with the `clientId` and Date.now value.
 *
 * @example generateIdFromUserAndDate({ clientId: 'foo' }) // -> 'uuid'
 * @param {object} obj the function main argument.
 * @param {string} obj.email the user email.
 * @param {string} obj.title the conference talk title.
 * @returns {string} the generated id
 */
function generateIdFromUserAndTitle(obj) {
  return compose(uuidWithNamespace, concatTitleWithEmail)(obj);
}

/**
 * Parse given HTTP Body object into a user object.
 *
 * @param {import('./create-talk-handler').Body} body HTTP Request parsed body.
 * @returns {Talk} a new user object.
 */
function applyTalkSpec(body) {
  return compose(
    rejectNilOrEmpty,
    applySpec({
      id: prop('id'),
      key: prop('key'),
      createdAt: () => new Date().toISOString(),
      speaker: compose(rejectNilOrEmpty, pick(['email', 'image', 'name']), propOr({}, 'speaker')),
      room: prop('room'),
      user: path(['speaker', 'email']),
      talkId: prop('talkId'),
      time: compose(date => date.toISOString(), parseISO, prop('time')),
      title: prop('title')
    })
  )(body);
}

/**
 * Creates a new conference talk entry in the DynamoDB table.
 * Before inserting a new record in the dB, it will check if the user already exist.
 * If the `talk` exists, it won't add a new record to the table and would response with a HTTP 409.
 * Otherwise, response will be HTTP 201 with recently created conference talk as a JSON response.
 *
 * @param {import('./create-talk-handler').AWSAPIGatewayEvent} event an AWS API Gateway event
 * @returns {Promise<import('./create-talk-handler').AWSAPIGatewayResponse>} API Gateway handler response
 */
async function createTalk(event) {
  const {
    body,
    requestContext: {
      authorizer: { user },
      flynamo: { insert, get },
      console
    }
  } = event;

  console.log('[create-talk] Received request with', body);

  throwErrorIfUserIsNotAdmin(user);

  const { email } = user;
  const talkId = generateIdFromUserAndTitle({ email, title: body.title });
  const id = getTalksCollectionId();
  const key = createTalksCollectionKey(talkId);

  console.debug(`[create-talk] Checking if talk with id ${id} and key ${key} already exist`);

  const talk = await get({ id, key });
  console.debug('[create-talk] Conference talk fetched from dB', talk);

  if (isNilOrEmpty(talk)) {
    let speaker = {};
    if (isNotNilOrEmpty(body.speaker)) {
      const id = getUsersCollectionId();
      const key = createUsersCollectionKey(body.speaker);

      console.debug(`[create-talk] Checking if speaker with id ${id} and key ${key} already exist`);
      speaker = await get({ id, key });

      throwErrorIfResourceIsNilOrEmpty(speaker);
    }

    const newTalk = applyTalkSpec({ ...body, speaker, id, key, talkId });

    console.log(`[create-talk] Creating a new conference talk in ${id} with key ${key}`);
    console.debug('[create-talk] Conference talk payload is', newTalk);

    await insert(newTalk);

    return {
      statusCode: CREATED,
      body: JSON.stringify(newTalk)
    };
  }

  return {
    statusCode: CONFLICT,
    body: JSON.stringify({ message: 'Conference talk already exist' })
  };
}

module.exports = createTalk;
