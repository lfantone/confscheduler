'use strict';

const { NO_CONTENT } = require('http-status-codes');
const {
  getTalksCollectionId,
  createStarTalksCollectionKey,
  throwErrorIfResourceIsNilOrEmpty
} = require('@confscheduler/toolbox');

/**
 * Delete an star to a conference talk.
 *
 * @param {import('./remove-talk-star-handler').AWSAPIGatewayEvent} event an AWS API Gateway event
 * @returns {Promise<import('./remove-talk-star-handler').AWSAPIGatewayResponse>} API Gateway handler response
 */
async function removeTalkStar(event) {
  const {
    body,
    pathParameters: { talkId },
    requestContext: {
      authorizer: { user },
      flynamo: { remove },
      console
    }
  } = event;

  console.log('[remove-talk-star] Received request with', body);

  const { email } = user;
  const id = getTalksCollectionId();
  const key = createStarTalksCollectionKey(talkId, email);

  console.debug(`[remove-talk-star] Removing star with id ${id} and key ${key}`);

  const star = await remove({ id, key });
  console.debug('[add-talk-star] Star removed from dB', star);

  throwErrorIfResourceIsNilOrEmpty(star);

  return {
    statusCode: NO_CONTENT
  };
}

module.exports = removeTalkStar;
