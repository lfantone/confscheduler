'use strict';
const { OK } = require('http-status-codes');
const {
  createTalksCollectionKey,
  getTalksCollectionId,
  throwErrorIfResourceIsNilOrEmpty
} = require('@confscheduler/toolbox');
const { retrieveStarCountFrom } = require('../core');

/**
 * Retrieve a conference talk entry by the given path parameter from the DynamoDB table.
 *
 * @param {import('./retrieve-talk-handler').AWSAPIGatewayEvent} event an AWS API Gateway event
 * @returns {Promise<import('./retrieve-talk-handler').AWSAPIGatewayResponse>} API Gateway handler response
 */
async function retrieveTalk(event) {
  const {
    pathParameters: { talkId },
    requestContext: {
      authorizer: { user },
      console,
      flynamo: { get, query }
    }
  } = event;
  const id = getTalksCollectionId();
  const key = createTalksCollectionKey(talkId);

  console.log(
    `[retrieve-talk] Received request to get a conference talk with id ${id} and key ${key}`
  );

  let talk = await get({ id, key });
  console.debug('[retrieve-talk] Conference talk retrieved.', talk);

  throwErrorIfResourceIsNilOrEmpty(talk);

  if (user.admin || (user.speaker && talk.user === user.email)) {
    console.debug('[retrieve-talk] Checking conference talk star count.');
    talk = await retrieveStarCountFrom(query, talk);
  }

  return {
    statusCode: OK,
    body: JSON.stringify(talk)
  };
}

module.exports = retrieveTalk;
