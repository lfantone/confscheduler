'use strict';
const schemas = require('./body-validation-schema');
const retrieveStarCountFrom = require('./retrieve-star-count-from');

module.exports = { ...schemas, retrieveStarCountFrom };
