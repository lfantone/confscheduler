'use strict';

const { mergeSpec } = require('@flybondi/ramda-land');
const { getTalksCollectionId, createStarTalksCollectionKey } = require('@confscheduler/toolbox');

/**
 * @typedef Talk
 * @type {object}
 * @property {string} talkId The talk uuid value.
 */

/**
 * @typedef ExtendedTalk
 * @type {object}
 * @property {string} talkId The talk uuid value.
 * @property {number} stars The talk star count.
 */

/**
 * Retrieve the star count from the given talk.
 *
 * @param {import('@confscheduler/toolbox/src/connectors/create-flynamo-connector').FlynamoAPI.query} query The dynamodb query client.
 * @param {Talk} talk A talk record.
 * @returns {ExtendedTalk} A talk with the `stars` property.
 */
async function retrieveStarCountFrom(query, talk) {
  const uuid = talk.talkId || talk.talkdId;
  const id = getTalksCollectionId();
  const key = createStarTalksCollectionKey(uuid);

  const params = {
    KeyConditionExpression: '#id = :id AND begins_with(#key, :key)',
    ExpressionAttributeNames: {
      '#id': 'id',
      '#key': 'key'
    },
    ExpressionAttributeValues: {
      ':id': { S: id },
      ':key': { S: key }
    }
  };

  const stars = await query(params);

  return mergeSpec({ stars: stars.length }, talk);
}

module.exports = retrieveStarCountFrom;
