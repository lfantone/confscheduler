'use strict';
const yup = require('yup');
const isUUID = require('is-uuid');
const { isAfter, isValid, parseISO } = require('date-fns/fp');
const { always, both, compose, ifElse } = require('ramda');
const { isNotNilOrEmpty } = require('@flybondi/ramda-land');

const isValidAndAfterNow = compose(both(isAfter(Date.now()), isValid), parseISO);
const runValidationIfNotNil = validation => ifElse(isNotNilOrEmpty, validation, always(true));

const talkSchema = yup.object().shape({
  title: yup.string().required(),
  time: yup.string().test('is-valid-date', isValidAndAfterNow).required(),
  speaker: yup.string().email().nullable(),
  room: yup.string().test('is-uuid-v5', runValidationIfNotNil(isUUID.v5)).nullable()
});

const updateTalkSchema = yup.object().shape({
  title: yup.string().nullable(),
  time: yup.string().test('is-valid-date', runValidationIfNotNil(isValidAndAfterNow)).nullable(),
  speaker: yup.string().email().nullable(),
  room: yup.string().test('is-uuid-v5', runValidationIfNotNil(isUUID.v5)).nullable(),
  talkId: yup.string().test('is-uuid-v5', isUUID.v5).required()
});

const talkIdSchema = yup.object().shape({
  talkId: yup.string().trim().test('is-uuid-v5', isUUID.v5).required()
});

module.exports = { talkSchema, talkIdSchema, updateTalkSchema };
