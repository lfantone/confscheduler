'use strict';
const { compose } = require('ramda');
const { isNilOrEmpty } = require('@flybondi/ramda-land');
const {
  createEventRequestContextInjector,
  createFlynamoConnectorFor,
  createLoggerFor,
  parseBody,
  yupValidation,
  tryCatch
} = require('@confscheduler/toolbox');
const updateTalk = require('./update-talk');
const { updateTalkSchema } = require('../core');

/**
 * Request path parameters
 *
 * @typedef {object} PathParameters
 * @property {string} talkId The conference talk uuid value.
 */

/**
 * Request body
 *
 * @typedef {object} Body
 * @property {string} [title] The conference talk title.
 * @property {string} [time] The conference talk start time in ISO String format.
 * @property {string} [speaker] The conference talk speaker email.
 * @property {string} [room] The conference talk room id.
 */

/**
 *
 * @typedef {object} RequestContext
 * @property {import('console')} console a native console proxy wrapper to enable/disable debug messages.
 * @property {import('@confscheduler/toolbox/src/connectors/create-flynamo-connector').FlynamoAPI} flynamo the initialised Flynamo client.
 */

/**
 * AWS API Gateway response object.
 *
 * @typedef {object} AWSAPIGatewayResponse
 * @property {number} statusCode HTTP Status code.
 * @property {string} body Handler response.
 */

/**
 * AWS API Gateway event.
 *
 * @typedef {object} AWSAPIGatewayEvent
 * @property {object} headers Incoming http request headers.
 * @property {RequestContext} requestContext Request global context.
 * @property {string} path Request URL path.
 * @property {string} httpMethod HTTP Request method.
 * @property {string} queryStringParameters HTTP Request query string parameters.
 * @property {Body} body HTTP Request body.
 * @property {PathParameters} pathParameters HTTP Request path parameters.
 */

/**
 * Builds AWS λ handler functions from the given `config` and injects required dependencies into their contexts.
 *
 * @param {object} config The service configuration object.
 * @param {string} config.logger The logger level value.
 * @param {string} config.table The platform table name
 * @returns {updateTalk} The AWS Lambda handler.
 */
function createUpdateTalkHandler(config) {
  const withLogger = createEventRequestContextInjector(() => createLoggerFor(config.logger));
  const withFlynamo = createEventRequestContextInjector(() =>
    createFlynamoConnectorFor(config.table)
  );

  return compose(
    tryCatch((res, err, statusCode, event) => {
      const {
        requestContext: { requestId },
        headers
      } = event;
      const token = headers.Authorization;

      console.error(
        `[update-talk][${statusCode}] An error occurred while processing the request to update a conference talk: ${err.message}`
      );
      console.debug(`[update-talk] Request (${requestId})`, token, err);
    }),
    parseBody,
    yupValidation(updateTalkSchema, (res, err, statusCode, event) => {
      console.error(
        `[update-talk] [${statusCode}] ${
          isNilOrEmpty(err.errors)
            ? `An unexpected error occurred: ${err.message}`
            : `Invalid request or missing arguments: ${JSON.stringify(err.errors)}`
        }`
      );
    }),
    withFlynamo(forTable => [forTable, { as: 'flynamo' }]),
    withLogger(console => [console, { as: 'console' }])
  )(updateTalk);
}

module.exports = createUpdateTalkHandler;
