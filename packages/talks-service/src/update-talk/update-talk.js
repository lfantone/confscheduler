'use strict';

const { CREATED, FORBIDDEN } = require('http-status-codes');
const { rejectNilOrEmpty, isNotNilOrEmpty, isNilOrEmpty } = require('@flybondi/ramda-land');
const {
  getTalksCollectionId,
  createTalksCollectionKey,
  getUsersCollectionId,
  createUsersCollectionKey,
  getRoomsCollectionId,
  createRoomsCollectionKey,
  throwErrorIf,
  throwErrorIfResourceIsNilOrEmpty
} = require('@confscheduler/toolbox');
const { applySpec, compose, pathOr, propOr, prop, pick, ifElse, always } = require('ramda');
const { parseISO } = require('date-fns/fp');

/**
 * @typedef User
 * @type {object}
 * @property {string} email The user email
 * @property {boolean} [admin] True when the user is an admin.
 * @property {boolean} [speaker] True when the user is an speaker.
 *
 */

/**
 * @typedef Talk
 * @type {object}
 * @property {string} id The talk collection id.
 * @property {string} key The talk collection key.
 * @property {string} createdAt An ISO Date string when the candidate was created.
 * @property {string} speaker The conference talk speaker email.
 * @property {string} room The conference talk room uuid.
 * @property {string} time The conference talk start time in ISO string format.
 *
 */

/**
 * Throws an error unless the `user.admin` property is not nil or empty in the context.
 *
 * @example
 *  throwErrorIfUserIsNotAdmin({ admin: null })  // --> throws FORBIDDEN error
 *  throwErrorIfUserIsNotAdmin({ admin: true })  // --> nothing!
 *
 * @function
 * @type {(user: User) => void}
 * @throws {Error}
 */
const throwErrorIfUserIsNotAdmin = throwErrorIf(
  'User is not an admin. Only admins are allowed to create a conference talk',
  'admin',
  FORBIDDEN
);

/**
 * Parse given HTTP Body object into a user object.
 *
 * @param {import('./update-talk-handler').Body} body HTTP Request parsed body.
 * @returns {Talk} a new user object.
 */
function applyTalkSpec(body) {
  return compose(
    rejectNilOrEmpty,
    applySpec({
      speaker: compose(
        ifElse(
          isNilOrEmpty,
          always(null),
          compose(rejectNilOrEmpty, pick(['email', 'image', 'name']))
        ),
        prop('speaker')
      ),
      room: propOr(null, 'room'),
      user: pathOr(null, ['speaker', 'email']),
      time: compose(
        ifElse(
          isNilOrEmpty,
          always(null),
          compose(date => date.toISOString(), parseISO)
        ),
        prop('time')
      ),
      title: propOr(null, 'title')
    })
  )(body);
}

/**
 * Creates a new conference talk entry in the DynamoDB table.
 * Before inserting a new record in the dB, it will check if the user already exist.
 * If the `talk` exists, it won't add a new record to the table and would response with a HTTP 409.
 * Otherwise, response will be HTTP 201 with recently created conference talk as a JSON response.
 *
 * @param {import('./update-talk-handler').AWSAPIGatewayEvent} event an AWS API Gateway event
 * @returns {Promise<import('./update-talk-handler').AWSAPIGatewayResponse>} API Gateway handler response
 */
async function updateTalk(event) {
  const {
    body,
    pathParameters: { talkId },
    requestContext: {
      authorizer: { user },
      flynamo: { update, get },
      console
    }
  } = event;

  console.log('[update-talk] Received request with', body);

  throwErrorIfUserIsNotAdmin(user);

  const id = getTalksCollectionId();
  const key = createTalksCollectionKey(talkId);

  console.debug(`[update-talk] Checking if talk with id ${id} and key ${key} already exist`);

  const talk = await get({ id, key });
  console.debug('[update-talk] Conference talk fetched from dB', talk);

  throwErrorIfResourceIsNilOrEmpty(talk);

  if (isNotNilOrEmpty(body.room)) {
    const id = getRoomsCollectionId();
    const key = createRoomsCollectionKey(body.room);

    console.debug(`[update-talk] Checking if room with id ${id} and key ${key} already exist`);
    const room = await get({ id, key });

    throwErrorIfResourceIsNilOrEmpty(room);
  }

  if (isNotNilOrEmpty(body.speaker)) {
    const id = getUsersCollectionId();
    const key = createUsersCollectionKey(body.speaker);
    console.debug(`[update-talk] Checking if speaker with id ${id} and key ${key} already exist`);
    const speaker = await get({
      id: getUsersCollectionId(),
      key: createUsersCollectionKey(body.speaker)
    });

    throwErrorIfResourceIsNilOrEmpty(speaker);
    body.speaker = speaker;
  }

  const newTalk = applyTalkSpec({ ...body, id, key });

  console.log(`[update-talk] Updating a conference talk ${id} with key ${key}`);
  console.debug('[update-talk] Conference talk payload is', newTalk);

  const updatedTalk = await update({ id, key }, newTalk);

  return {
    statusCode: CREATED,
    body: JSON.stringify(updatedTalk)
  };
}

module.exports = updateTalk;
