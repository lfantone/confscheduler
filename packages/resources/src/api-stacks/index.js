'use strict';

const RoomsStack = require('./rooms-stack');
const TalksStack = require('./talks-stack');
const UsersStack = require('./users-stack');

module.exports = {
  RoomsStack,
  TalksStack,
  UsersStack
};
