'use strict';
const { LambdaIntegration, RestApi } = require('@aws-cdk/aws-apigateway');
const { AssetCode, Function, Runtime } = require('@aws-cdk/aws-lambda');
const { NestedStack } = require('@aws-cdk/core');

class UsersAPIStack extends NestedStack {
  constructor(scope, options, config = {}) {
    super(scope, 'user-api-construct-id', config.stack);

    const namespace = config.namespace || 'conference-scheduler';
    const {
      role,
      table,
      api: { id, resourceId }
    } = options;
    const code = new AssetCode('../users-service');
    const createUserHandler = new Function(this, `${namespace}-create-user-handler`, {
      ...config.lambda,
      code,
      handler: 'index.createUserHandler',
      runtime: Runtime.NODEJS_12_X,
      memorySize: 256,
      environment: {
        TABLE_NAME: table.tableName
      },
      role
    });

    const retrieveUserHandler = new Function(this, `${namespace}-retrieve-user-handler`, {
      ...config.lambda,
      code,
      handler: 'index.retrieveUserHandler',
      runtime: Runtime.NODEJS_12_X,
      memorySize: 256,
      environment: {
        TABLE_NAME: table.tableName
      },
      role
    });

    const authUserHandler = new Function(this, `${namespace}-authenticate-user-handler`, {
      ...config.lambda,
      code,
      handler: 'index.authenticateUserHandler',
      runtime: Runtime.NODEJS_12_X,
      memorySize: 256,
      environment: {
        TABLE_NAME: table.tableName
      },
      role
    });

    table.grantReadWriteData(createUserHandler);
    table.grantReadWriteData(retrieveUserHandler);
    table.grantReadWriteData(authUserHandler);

    const api = RestApi.fromRestApiAttributes(this, `${namespace}-rest-api-id`, {
      restApiId: id,
      rootResourceId: resourceId
    });

    const users = api.root.addResource('users');
    const auth = users.addResource('auth');

    const postUser = users.addMethod('POST', new LambdaIntegration(createUserHandler));
    const getUser = users.addMethod('GET', new LambdaIntegration(retrieveUserHandler));
    const postAuth = auth.addMethod('POST', new LambdaIntegration(authUserHandler));

    this.functions = [createUserHandler, retrieveUserHandler, authUserHandler];
    this.methods = [postUser, getUser, postAuth];
  }
}

module.exports = UsersAPIStack;
