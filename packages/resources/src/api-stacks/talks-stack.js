'use strict';
const { LambdaIntegration, RestApi } = require('@aws-cdk/aws-apigateway');
const { AssetCode, Function, Runtime } = require('@aws-cdk/aws-lambda');
const { NestedStack } = require('@aws-cdk/core');

class TalksStack extends NestedStack {
  constructor(scope, options, config = {}) {
    super(scope, 'talks-api-stack', config.stack);

    const namespace = config.namespace || 'conference-scheduler';
    const {
      role,
      table,
      api: { id, resourceId }
    } = options;
    const code = new AssetCode('../talks-service');
    const createTalkHandler = new Function(this, `${namespace}-create-talk-handler`, {
      ...config.lambda,
      code,
      handler: 'index.createTalkHandler',
      runtime: Runtime.NODEJS_12_X,
      memorySize: 256,
      environment: {
        TABLE_NAME: table.tableName
      },
      role
    });

    const retrieveTalkHandler = new Function(this, `${namespace}-retrieve-talk-handler`, {
      ...config.lambda,
      code,
      handler: 'index.retrieveTalkHandler',
      runtime: Runtime.NODEJS_12_X,
      memorySize: 256,
      environment: {
        TABLE_NAME: table.tableName
      },
      role
    });

    const retrieveAllTalksHandler = new Function(this, `${namespace}-retrieve-all-talks-handler`, {
      ...config.lambda,
      code,
      handler: 'index.retrieveAllTalksHandler',
      runtime: Runtime.NODEJS_12_X,
      memorySize: 256,
      environment: {
        TABLE_NAME: table.tableName
      }
    });

    const updateTalkHandler = new Function(this, `${namespace}-update-talk-handler`, {
      ...config.lambda,
      code,
      handler: 'index.updateTalkHandler',
      runtime: Runtime.NODEJS_12_X,
      memorySize: 256,
      environment: {
        TABLE_NAME: table.tableName
      }
    });

    const addStarTalkHandler = new Function(this, `${namespace}-add-star-talk-handler`, {
      ...config.lambda,
      code,
      handler: 'index.addStarTalkHandler',
      runtime: Runtime.NODEJS_12_X,
      memorySize: 256,
      environment: {
        TABLE_NAME: table.tableName
      }
    });

    const removeStarTalkHandler = new Function(this, `${namespace}-remove-star-talk-handler`, {
      ...config.lambda,
      code,
      handler: 'index.removeStarTalkHandler',
      runtime: Runtime.NODEJS_12_X,
      memorySize: 256,
      environment: {
        TABLE_NAME: table.tableName
      }
    });

    table.grantReadWriteData(createTalkHandler);
    table.grantReadWriteData(retrieveTalkHandler);
    table.grantReadWriteData(retrieveAllTalksHandler);
    table.grantReadWriteData(updateTalkHandler);
    table.grantReadWriteData(addStarTalkHandler);
    table.grantReadWriteData(removeStarTalkHandler);

    const api = RestApi.fromRestApiAttributes(this, `${namespace}-rest-api-id`, {
      restApiId: id,
      rootResourceId: resourceId
    });

    const talks = api.root.addResource('talks');
    const talk = talks.addResource('{talkId}');
    const stars = talk.addResource('stars');

    const postTalk = talks.addMethod('POST', new LambdaIntegration(createTalkHandler));
    const getAllTalks = talks.addMethod('GET', new LambdaIntegration(retrieveAllTalksHandler));
    const getTalks = talk.addMethod('GET', new LambdaIntegration(retrieveTalkHandler));
    const putTalk = talk.addMethod('PUT', new LambdaIntegration(updateTalkHandler));
    const postStarTalk = stars.addMethod('POST', new LambdaIntegration(addStarTalkHandler));
    const deleteStarTalk = stars.addMethod('DELETE', new LambdaIntegration(removeStarTalkHandler));

    this.functions = [
      createTalkHandler,
      retrieveTalkHandler,
      retrieveAllTalksHandler,
      updateTalkHandler,
      addStarTalkHandler,
      removeStarTalkHandler
    ];
    this.methods = [postTalk, getAllTalks, getTalks, putTalk, postStarTalk, deleteStarTalk];
  }
}

module.exports = TalksStack;
