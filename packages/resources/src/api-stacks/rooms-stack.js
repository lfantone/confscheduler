'use strict';
const { LambdaIntegration, RestApi } = require('@aws-cdk/aws-apigateway');
const { AssetCode, Function, Runtime } = require('@aws-cdk/aws-lambda');
const { NestedStack } = require('@aws-cdk/core');

class RoomsStack extends NestedStack {
  constructor(scope, options, config = {}) {
    super(scope, 'rooms-api-stack', config.stack);

    const namespace = config.namespace || 'conference-scheduler';
    const {
      role,
      table,
      api: { id, resourceId }
    } = options;
    const code = new AssetCode('../rooms-service');
    const createRoomHandler = new Function(this, `${namespace}-create-room-handler`, {
      ...config.lambda,
      code,
      handler: 'index.createRoomHandler',
      runtime: Runtime.NODEJS_12_X,
      memorySize: 256,
      environment: {
        TABLE_NAME: table.tableName
      },
      role
    });

    const retrieveAllRoomsHandler = new Function(this, `${namespace}-retrieve-all-rooms-handler`, {
      ...config.lambda,
      code,
      handler: 'index.retrieveAllTalksHandler',
      runtime: Runtime.NODEJS_12_X,
      memorySize: 256,
      environment: {
        TABLE_NAME: table.tableName
      }
    });

    const retrieveRoomHandler = new Function(this, `${namespace}-retrieve-room-handler`, {
      ...config.lambda,
      code,
      handler: 'index.retrieveRoomHandler',
      runtime: Runtime.NODEJS_12_X,
      memorySize: 256,
      environment: {
        TABLE_NAME: table.tableName
      }
    });

    table.grantReadWriteData(createRoomHandler);
    table.grantReadWriteData(retrieveAllRoomsHandler);
    table.grantReadWriteData(retrieveRoomHandler);

    const api = RestApi.fromRestApiAttributes(this, `${namespace}-rest-api-id`, {
      restApiId: id,
      rootResourceId: resourceId
    });

    const rooms = api.root.addResource('rooms');
    const room = rooms.addResource('{roomId}');

    const postRoom = rooms.addMethod('POST', new LambdaIntegration(createRoomHandler));
    const getAllRooms = rooms.addMethod('GET', new LambdaIntegration(retrieveAllRoomsHandler), {
      requestParameters: { 'method.request.querystring.available': true }
    });
    const getRoom = room.addMethod('GET', new LambdaIntegration(retrieveRoomHandler));

    this.functions = [createRoomHandler, retrieveAllRoomsHandler, retrieveRoomHandler];
    this.methods = [postRoom, getAllRooms, getRoom];
  }
}

module.exports = RoomsStack;
