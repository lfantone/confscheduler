'use strict';
const { getUsersCollectionId, createUsersCollectionKey } = require('@confscheduler/toolbox');
const { compose, applySpec, prop, ifElse, equals, toLower, always, map } = require('ramda');
const { rejectNilOrEmpty } = require('@flybondi/ramda-land');

const USERS = [
  {
    email: 'user@agilesearch.io',
    name: 'John Doe',
    type: 'user'
  },
  {
    email: 'admin@agilesearch.io',
    name: 'Admin',
    type: 'admin'
  },
  {
    email: 'tom.occhino@agilesearch.io',
    name: 'Tom Occhino',
    type: 'speaker'
  },
  {
    email: 'frank.yan@agilesearch.io',
    name: 'Frank Yan',
    type: 'speaker'
  },
  {
    email: 'tejas.kumar@agilesearch.io',
    name: 'Tejas Kumar',
    type: 'speaker'
  },
  {
    email: 'brian.vaughn@agilesearch.io',
    name: 'Brian Vaughn',
    type: 'speaker'
  },
  {
    email: 'cameron.yick@agilesearch.io',
    name: 'Cameron Yick',
    type: 'speaker'
  }
];

function applyUserSpec(body) {
  return compose(
    rejectNilOrEmpty,
    applySpec({
      id: getUsersCollectionId,
      key: compose(createUsersCollectionKey, prop('email')),
      admin: ifElse(compose(equals('admin'), toLower, prop('type')), always(true), always(null)),
      createdAt: () => new Date().toISOString(),
      email: prop('email'),
      image: compose(seed => `https://api.adorable.io/avatars/175/${seed}`, prop('email')),
      name: prop('name'),
      speaker: ifElse(compose(equals('speaker'), toLower, prop('type')), always(true), always(null))
    })
  )(body);
}

function generateUsers() {
  return map(applyUserSpec, USERS);
}

module.exports = generateUsers;
