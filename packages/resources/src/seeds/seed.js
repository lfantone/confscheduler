'use strict';
const { createFlynamoConnectorFor } = require('@confscheduler/toolbox');
const generateUsers = require('./seed-users');
const generateTalks = require('./seed-talks');
const generateRooms = require('./seed-rooms');
const { flatten } = require('ramda');

const { batchInsert } = createFlynamoConnectorFor('development-conference-scheduler');

async function seed() {
  console.log('Seeding dynamodb table');
  const users = generateUsers();
  console.log('Generated users are: ', users);

  const talks = generateTalks();
  console.log('Generated talks are: ', talks);

  const rooms = generateRooms();
  console.log('Generated rooms are: ', rooms);

  const records = flatten([users, talks, rooms]);

  await batchInsert(records);
}

seed()
  .then(() => console.log('Done'))
  .catch(console.error);
