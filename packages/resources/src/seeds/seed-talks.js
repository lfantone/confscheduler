'use strict';
const { getTalksCollectionId, createTalksCollectionKey } = require('@confscheduler/toolbox');
const {
  applySpec,
  compose,
  concat,
  converge,
  curryN,
  flip,
  map,
  path,
  pick,
  prop,
  propOr
} = require('ramda');
const { rejectNilOrEmpty } = require('@flybondi/ramda-land');
const { parseISO } = require('date-fns/fp');
const { v5 } = require('uuid');

const NAMESPACE = '4b699b32-7e87-4be0-99a4-1eeb5d41a94f';

const TALKS = [
  {
    title: 'Registration & Check-in',
    time: '2020-08-29T18:00:00Z',
    speaker: null,
    room: null,
    email: 'admin@agilesearch.io'
  },
  {
    title: 'Welcome Reception',
    time: '2020-08-29T19:00:00Z',
    speaker: null,
    room: null,
    email: 'admin@agilesearch.io'
  },
  {
    title: 'Sunrise Yoga',
    time: '2020-08-30T06:30:00Z',
    speaker: null,
    room: null,
    email: 'admin@agilesearch.io'
  },
  {
    title: 'Breakfast',
    time: '2020-08-30T07:00:00Z',
    speaker: null,
    room: null,
    email: 'admin@agilesearch.io'
  },
  {
    title: 'Registration',
    time: '2020-08-30T07:30:00Z',
    speaker: null,
    room: null,
    email: 'admin@agilesearch.io'
  },
  {
    title: 'Keynote',
    time: '2020-08-30T09:00:00Z',
    speaker: {
      email: 'tom.occhino@agilesearch.io',
      name: 'Tom Occhino',
      image: 'https://api.adorable.io/avatars/175/tom.occhino@agilesearch.io'
    },
    room: null,
    email: 'admin@agilesearch.io'
  },
  {
    title: 'Building the new web with Vuejs and AWS',
    time: '2020-08-30T10:00:00Z',
    speaker: {
      email: 'frank.yan@agilesearch.io',
      name: 'Frank Yan',
      image: 'https://api.adorable.io/avatars/175/frank.yan@agilesearch.io'
    },
    room: null,
    email: 'admin@agilesearch.io'
  },
  {
    title: 'Break',
    time: '2020-08-30T11:30:00Z',
    speaker: null,
    room: null,
    email: 'admin@agilesearch.io'
  },
  {
    title: 'Using hooks and codegen to bring the benefits of GraphQL to REST APIs',
    time: '2020-08-30T12:00:00Z',
    speaker: {
      email: 'tejas.kumar@agilesearch.io',
      name: 'Tejas Kumar',
      image: 'https://api.adorable.io/avatars/175/tejas.kumar@agilesearch.io'
    },
    room: null,
    email: 'admin@agilesearch.io'
  },
  {
    title: 'Breakfast reloaded',
    time: '2020-08-31T07:00:00Z',
    speaker: null,
    room: null,
    email: 'admin@agilesearch.io'
  },
  {
    title: 'Vuejs developer tooling',
    time: '2020-08-31T09:00:00Z',
    speaker: {
      email: 'brian.vaughn@agilesearch.io',
      name: 'Brian Vaughn',
      image: 'https://api.adorable.io/avatars/175/brian.vaughn@agilesearch.io'
    },
    room: null,
    email: 'admin@agilesearch.io'
  },
  {
    title: 'Automatic visualizations of the frontend',
    time: '2020-08-31T11:00:00Z',
    speaker: {
      email: 'cameron.yick@agilesearch.io',
      name: 'Cameron Yick',
      image: 'https://api.adorable.io/avatars/175/cameron.yick@agilesearch.io'
    },
    room: null,
    email: 'admin@agilesearch.io'
  }
];

const uuidWithNamespace = flip(curryN(2, v5))(NAMESPACE);
const concatTitleWithEmail = converge(concat, [prop('title'), prop('email')]);
function generateIdFromUserAndTitle(obj) {
  return compose(uuidWithNamespace, concatTitleWithEmail)(obj);
}

function applyTalkSpec(body) {
  return compose(
    rejectNilOrEmpty,
    applySpec({
      id: getTalksCollectionId,
      key: compose(createTalksCollectionKey, generateIdFromUserAndTitle),
      createdAt: () => new Date().toISOString(),
      speaker: compose(rejectNilOrEmpty, pick(['email', 'image', 'name']), propOr({}, 'speaker')),
      room: prop('room'),
      user: path(['speaker', 'email']),
      talkId: generateIdFromUserAndTitle,
      time: compose(date => date.toISOString(), parseISO, prop('time')),
      title: prop('title')
    })
  )(body);
}

function generateTalks() {
  return map(applyTalkSpec, TALKS);
}

module.exports = generateTalks;
