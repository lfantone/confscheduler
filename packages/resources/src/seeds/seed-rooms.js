'use strict';
const { isNilOrEmpty, rejectNilOrEmpty } = require('@flybondi/ramda-land');
const { getRoomsCollectionId, createRoomsCollectionKey } = require('@confscheduler/toolbox');
const {
  applySpec,
  compose,
  map,
  prop,
  flip,
  curryN,
  converge,
  concat,
  when,
  always
} = require('ramda');
const { v5 } = require('uuid');

const NAMESPACE = '4b699b32-7e87-4be0-99a4-1eeb5d41a94f';

const ROOMS = [
  {
    title: 'Combusken #256',
    description:
      'Combusken toughens up its legs and thighs by running through fields and mountains.',
    talkId: null
  },
  {
    title: 'Charmeleon #005',
    description:
      'It has a barbaric nature. In battle, it whips its fiery tail around and slashes away with sharp claws',
    talkId: null
  },
  {
    title: 'Gyarados #130',
    description:
      'It has an extremely aggressive nature. The Hyper Beam it shoots from its mouth totally incinerates all targets.',
    talkId: null
  },
  {
    title: 'Metapod #011',
    description:
      'It is waiting for the moment to evolve. At this stage, it can only harden, so it remains motionless to avoid attack.',
    talkId: null
  }
];

const uuidWithNamespace = flip(curryN(2, v5))(NAMESPACE);
const dateNowString = compose(toString, Date.now);
const concatTitleWithDate = converge(concat, [prop('title'), dateNowString]);

function generateIdFromTitle(obj) {
  return compose(uuidWithNamespace, concatTitleWithDate)(obj);
}

function applyRoomSpec(body) {
  return compose(
    rejectNilOrEmpty,
    applySpec({
      id: getRoomsCollectionId,
      key: compose(createRoomsCollectionKey, generateIdFromTitle),
      createdAt: () => new Date().toISOString(),
      title: prop('title'),
      description: prop('description'),
      creator: prop('email'),
      talkId: compose(when(isNilOrEmpty, always(null)), prop('talkId')),
      roomId: generateIdFromTitle,
      image: compose(
        seed => `https://picsum.photos/seed/${seed}/200/300?greyscale`,
        generateIdFromTitle
      )
    })
  )(body);
}

function generateRooms() {
  return map(applyRoomSpec, ROOMS);
}

module.exports = generateRooms;
