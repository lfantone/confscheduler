'use strict';
const { Table, AttributeType, BillingMode, ProjectionType } = require('@aws-cdk/aws-dynamodb');
const { NestedStack } = require('@aws-cdk/core');

class DatabaseStack extends NestedStack {
  constructor(scope, config = {}) {
    super(scope, 'dynamodb-stack', config.stack);

    const environment = config.stage || 'development';
    const namespace = config.namespace || 'conference-scheduler';

    const table = new Table(this, `${namespace}-table-id`, {
      ...config.dynamodb,
      tableName: `${environment}-${namespace}`,
      partitionKey: {
        name: 'id',
        type: AttributeType.STRING
      },
      sortKey: {
        name: 'key',
        type: AttributeType.STRING
      },
      billingMode: BillingMode.PAY_PER_REQUEST
    });

    table.addGlobalSecondaryIndex({
      indexName: 'room-talks-index',
      partitionKey: {
        name: 'roomId',
        type: AttributeType.STRING
      },
      sortKey: {
        name: 'talkId',
        type: AttributeType.STRING
      },
      projectionType: ProjectionType.ALL
    });

    table.addGlobalSecondaryIndex({
      indexName: 'user-stars-index',
      partitionKey: {
        name: 'user',
        type: AttributeType.STRING
      },
      sortKey: {
        name: 'talkId',
        type: AttributeType.STRING
      },
      projectionType: ProjectionType.ALL
    });

    this.table = table;
  }
}

module.exports = DatabaseStack;
