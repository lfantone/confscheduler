'use strict';

const { Stack } = require('@aws-cdk/core');
const { RestApi, Deployment, Stage } = require('@aws-cdk/aws-apigateway');
const { Role, ManagedPolicy, ServicePrincipal } = require('@aws-cdk/aws-iam');
const { RoomsStack, TalksStack, UsersStack } = require('./api-stacks');
const DatabaseStack = require('./create-db-stack');
// const addCorsOptions = require('./add-cors');

class ConferenceSchedulerStack extends Stack {
  constructor(app, id, config) {
    super(app, id, config.stack);

    const environment = config.stage || 'development';
    const namespace = config.namespace || 'conference-schedules';

    const database = new DatabaseStack(this, config);

    const role = new Role(this, `${namespace}-lambda-role-id`, {
      assumedBy: new ServicePrincipal('lambda.amazonaws.com'),
      description: 'Role to be used by lambda functions',
      roleName: 'lambda-role',
      managedPolicies: [
        ManagedPolicy.fromAwsManagedPolicyName('service-role/AWSLambdaBasicExecutionRole')
      ]
    });

    const api = new RestApi(this, `${namespace}-rest-api-id`, {
      deploy: false
    });
    api.root.addMethod('ANY');

    const rooms = new RoomsStack(
      this,
      {
        role,
        table: database.table,
        api: { id: api.restApiId, resourceId: api.restApiRootResourceId }
      },
      config
    );

    const users = new UsersStack(
      this,
      {
        role,
        table: database.table,
        api: { id: api.restApiId, resourceId: api.restApiRootResourceId }
      },
      config
    );
    const talks = new TalksStack(
      this,
      {
        role,
        table: database.table,
        api: { id: api.restApiId, resourceId: api.restApiRootResourceId }
      },
      config
    );

    const deployment = new Deployment(this, `${namespace}-deployment-id`, {
      api
    });

    this.addDeploymentNodeDependency(deployment, rooms.methods);
    this.addDeploymentNodeDependency(deployment, users.methods);
    this.addDeploymentNodeDependency(deployment, talks.methods);

    this.stage = new Stage(this, `${namespace}-stage-id`, { deployment, stageName: environment });
  }

  addDeploymentNodeDependency(deployment, methods = []) {
    methods.forEach(method => deployment.node.addDependency(method));
  }
}

function createAppStack(app, config) {
  return new ConferenceSchedulerStack(app, 'ConferenceSchedulerStack', config);
}

module.exports = createAppStack;
