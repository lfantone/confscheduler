const config = require('../../eslint.config.js');

module.exports = {
  ...config,
  root: true
};
