# ConfScheduler

## The assignment

The organization Barbarados 15-15 is organizing a conference focusing on software development.

The conference is 2 days long and multitracked.

They need you help building a application that will be used by all attendees during the conference.

## The goal

The goal for this exercise is to make you practice some Vue programming together with a backend in aws.

The assignment (described below) is a minimal set of the requirements. Use you own thoughts to create a good application for Barbarados 15-15:s needs.

However focus on create a working application with good code and architecture. If the requirement are actually met is secondary.

### Frontend (Vue)

The focus for this task is creating beautiful and scalable vue code.

When you are doing the assignment always imaging you are working on a project that will scale so extract all things you consider reusable.

Use the Vue CLI to easily getting started with the client.

The styling isn't that important, however its nice it you use some 3:th part lib like Vuetify or Vue Material Kit.

### Backend (AWS)

Use AWS CDK to create your infrastructure as IAC.

Use API Gateway + Lambda + DynamoDB.

Focus on creating a good datamodeling for the dynamodb for the current access patterns.

## Login

All users should be able to login.

This it the first view.

For this demo there isn't a need to add password.

The username if the unique identifier for that user in the database.

## The scheduler

This is the first screen for all user types after login.

In this view you see all talks during the conference.

This view is used to get a overview of all the sessions.

- All talks that are in the same slot (same time) should be aligned on the x-axis
- Talks can have a room assigned to it, then they should be aligned on the y-axis
- **Participant** should be able to **star** a talk
- **Admins** should be able to see the number of **stars** per talk.
- **Speakers** should be able to see the number of **stars** to there own talk.

## Talk details

In this view you can see the details regarding a talk.

- The **Speaker** should be able to update the description of the talk
- **Participant** should be able to **star** a talk
- **Admins** should be able to see the number of **stars** per talk.
- **Admins** should be able to assign a talk to a room.
- **Speakers** should be able to see the number of **stars** to there own talk.

## My stars view (My schedule)

There a user can see all the talks they have starred.

- You should only see your own stars
- For each starred talk you should be able to see the time, title and speaker.
- If a user have starred two talks in the same timeslot, the user should be informed.
- If a user haven't starred any talks in the a timeslot, the user should be informed.
